# TP3 Admin : Vagrant

## Sommaire

- [TP3 Admin : Vagrant](#tp3-admin--vagrant)
  - [Sommaire](#sommaire)
- [0. Setup](#0-setup)
  - [Sommaire](#sommaire-1)
  - [0. Intro blabla](#0-intro-blabla)
  - [1. Une première VM](#1-une-première-vm)
  - [2. Repackaging](#2-repackaging)
  - [3. Moult VMs](#3-moult-vms)


## 1. Une première VM

🌞 **Générer un `Vagrantfile`**

- vous bosserez avec cet OS pour le restant du TP
- vous pouvez générer une `Vagrantfile` fonctionnel pour une box donnée avec les commandes :

```bash
# création d'un répertoire de travail
$ mkdir ~/work/vagrant/test
$ cd ~/work/vagrant/test

# génération du Vagrantfile
$ vagrant init <NOM_DE_LA_BOX>

# on peut constater d'un Vagrantfile a été créé
$ ls
Vagrantfile
$ cat Vagrantfile
[...]
```

```
PS C:\Users\ZerBr\linux-b2> vagrant init generic/rocky9
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.

PS C:\Users\ZerBr\linux-b2> ls .\Vagrantfile


    Répertoire : C:\Users\ZerBr\linux-b2


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        18/01/2024     10:34           3461 Vagrantfile
```

🌞 **Modifier le `Vagrantfile`**

- les lignes qui suivent doivent être ajouter dans le bloc où l'objet `config` est défini
- ajouter les lignes suivantes :

```ruby
# Désactive les updates auto qui peuvent ralentir le lancement de la machine
config.vm.box_check_update = false 

# La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
config.vm.synced_folder ".", "/vagrant", disabled: true
```

```
PS C:\Users\ZerBr\linux-b2> cat .\Vagrantfile | findstr /n "config.vm.synced_folder"
53:  config.vm.synced_folder ".", "/vagrant", disabled: true

PS C:\Users\ZerBr\linux-b2> cat .\Vagrantfile | findstr /n "config.vm.box_check"
20:  config.vm.box_check_update = false
```

🌞 **Faire joujou avec une VM**

```bash
# on peut allumer tout de suite une VM issue de cette box, le Vagrantfile est censé être fonctionnel
$ vagrant up

# une fois la VM allumée...
$ vagrant status
$ vagrant ssh


# on peut éteindre la VM avec
$ vagrant halt

# et la détruire avec
$ vagrant destroy -f
```

```
PS C:\Users\ZerBr\linux-b2> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'generic/rocky9' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: >= 0
==> default: Loading metadata for box 'generic/rocky9'
    default: URL: https://vagrantcloud.com/api/v2/vagrant/generic/rocky9
==> default: Adding box 'generic/rocky9' (v4.3.2) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/generic/boxes/rocky9/versions/4.3.2/providers/virtualbox/unknown/vagrant.box
    default:
    default: Calculating and comparing box checksum...
==> default: Successfully added box 'generic/rocky9' (v4.3.2) for 'virtualbox'!
==> default: Importing base box 'generic/rocky9'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'generic/rocky9' version '4.3.2' is up to date...
==> default: Setting the name of the VM: linux-b2_default_1705569830542_96554
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: The guest additions on this VM do not match the installed version of
    default: VirtualBox! In most cases this is fine, but in rare cases it can
    default: prevent things such as shared folders from working properly. If you see
    default: shared folder errors, please make sure the guest additions within the
    default: virtual machine match the version of VirtualBox you have installed on
    default: your host and reload your VM.
    default:
    default: Guest Additions Version: 6.1.46
    default: VirtualBox Version: 7.0

PS C:\Users\ZerBr\linux-b2> vagrant status
Current machine states:

default                   running (virtualbox)

The VM is running. To stop this VM, you can run `vagrant halt` to
shut it down forcefully, or you can run `vagrant suspend` to simply
suspend the virtual machine. In either case, to restart it again,
simply run `vagrant up`.

PS C:\Users\ZerBr\linux-b2> vagrant ssh
[vagrant@rocky9 ~]$

PS C:\Users\ZerBr\linux-b2> vagrant halt
==> default: Attempting graceful shutdown of VM...

PS C:\Users\ZerBr\linux-b2> vagrant destroy -f
==> default: Destroying VM and associated drives...
```

## 2. Repackaging

🌞 **Repackager la box que vous avez choisie**

- elle doit :
  - être à jour
  - disposer des commandes `vim`, `ip`, `dig`, `ss`, `nc`
  - avoir un firewall actif
  - SELinux (systèmes RedHat) et/ou AppArmor (plutôt sur Ubuntu) désactivés
- pour repackager une box, vous pouvez utiliser les commandes suivantes :

```bash
# On convertit la VM en un fichier .box sur le disque
# Le fichier est créé dans le répertoire courant si on ne précise pas un chemin explicitement
$ vagrant package --output super_box.box

# On ajoute le fichier .box à la liste des box que gère Vagrant
$ vagrant box add super_box super_box.box

# On devrait voir la nouvelle box dans la liste locale des boxes de Vagrant
$ vagrant box list
```

```
[vagrant@rocky9 ~]$ sudo dnf update
Extra Packages for Enterprise Linux 9 - x86_64                    4.4 MB/s |  20 MB     00:04
Extra Packages for Enterprise Linux 9 openh264 (From Cisco) - x86 2.9 kB/s | 2.5 kB     00:00
Rocky Linux 9 - BaseOS                                            2.3 MB/s | 2.2 MB     00:00
Rocky Linux 9 - AppStream                                         3.7 MB/s | 7.4 MB     00:01
Rocky Linux 9 - Extras                                             23 kB/s |  14 kB     00:00
Dependencies resolved.

[...]

Complete!

[vagrant@rocky9 ~]$ sudo dnf install vim
Extra Packages for Enterprise Linux 9 - x86_64                     54 kB/s |  33 kB     00:00
Package vim-enhanced-2:8.2.2637-20.el9_1.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!

[vagrant@rocky9 ~]$ sudo dnf install iproute
Last metadata expiration check: 0:03:02 ago on Thu 18 Jan 2024 10:17:34 AM UTC.
Package iproute-6.2.0-5.el9.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!

[vagrant@rocky9 ~]$ sudo dnf install bind-utils
Last metadata expiration check: 0:03:49 ago on Thu 18 Jan 2024 10:17:34 AM UTC.
Package bind-utils-32:9.16.23-14.el9_3.0.1.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!

[vagrant@rocky9 ~]$ sudo dnf install iproute nc
Extra Packages for Enterprise Linux 9 - x86_64                    100 kB/s |  33 kB     00:00
Package iproute-6.2.0-5.el9.x86_64 is already installed.
Dependencies resolved.
==================================================================================================
 Package               Architecture       Version                     Repository             Size
==================================================================================================
Installing:
 nmap-ncat             x86_64             3:7.92-1.el9                appstream             222 k

Transaction Summary
==================================================================================================
Install  1 Package

Total download size: 222 k
Installed size: 469 k
Is this ok [y/N]: y
Downloading Packages:
nmap-ncat-7.92-1.el9.x86_64.rpm                                   209 kB/s | 222 kB     00:01
--------------------------------------------------------------------------------------------------
Total                                                             158 kB/s | 222 kB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                          1/1
  Installing       : nmap-ncat-3:7.92-1.el9.x86_64                                            1/1
  Running scriptlet: nmap-ncat-3:7.92-1.el9.x86_64                                            1/1
  Verifying        : nmap-ncat-3:7.92-1.el9.x86_64                                            1/1

Installed:
  nmap-ncat-3:7.92-1.el9.x86_64

Complete!

[vagrant@rocky9 ~]$ sudo systemctl start firewalld
[vagrant@rocky9 ~]$ sudo systemctl enable firewalld
[vagrant@rocky9 ~]$ sudo systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
     Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; preset: enabled)
     Active: active (running) since Thu 2024-01-18 10:16:31 UTC; 14min ago
       Docs: man:firewalld(1)
   Main PID: 44623 (firewalld)
      Tasks: 2 (limit: 11051)
     Memory: 27.8M
        CPU: 620ms
     CGroup: /system.slice/firewalld.service
             └─44623 /usr/bin/python3 -s /usr/sbin/firewalld --nofork --nopid

[vagrant@rocky9 ~]$ sudo setenforce 0

[vagrant@rocky9 ~]$ sudo cat /etc/selinux/config | grep -n "SELINUX=di"
22:SELINUX=disabled

PS C:\Users\ZerBr\linux-b2> vagrant package --output super_box.box
==> default: Attempting graceful shutdown of VM...
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: C:/Users/ZerBr/linux-b2/super_box.box

PS C:\Users\ZerBr\linux-b2> vagrant box add super_box super_box.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'super_box' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/ZerBr/linux-b2/super_box.box
    box:
==> box: Successfully added box 'super_box' (v0) for ''!

PS C:\Users\ZerBr\linux-b2> vagrant box list
generic/rocky9     (virtualbox, 4.3.2)
hashicorp/bionic64 (virtualbox, 1.0.282)
super_box          (virtualbox, 0)
```

🌞 **Ecrivez un `Vagrantfile` qui lance une VM à partir de votre Box**

- et testez que ça fonctionne !

```
PS C:\Users\ZerBr\linux-b2\VagrantF2> vagrant init super_box
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.

PS C:\Users\ZerBr\linux-b2\VagrantF2> cat .\Vagrantfile | findstr /n "config.vm.box ="
2:# vi: set ft=ruby :
15:  config.vm.box = "super_box"
PS C:\Users\ZerBr\linux-b2\VagrantF2> cat .\Vagrantfile | findstr /n "Vagrant.configure"
4:# All Vagrant configuration is done below. The "2" in Vagrant.configure
8:Vagrant.configure("2") do |config|

PS C:\Users\ZerBr\linux-b2\VagrantF2> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'super_box'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: VagrantF2_default_1705576525180_41627
Vagrant is currently configured to create VirtualBox synced folders with
the `SharedFoldersEnableSymlinksCreate` option enabled. If the Vagrant
guest is not trusted, you may want to disable this option. For more
information on this option, please refer to the VirtualBox manual:
[...]

PS C:\Users\ZerBr\linux-b2\VagrantF2> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'super_box'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: VagrantF2_default_1705577598072_3424
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: The guest additions on this VM do not match the installed version of
    default: VirtualBox! In most cases this is fine, but in rare cases it can
    default: prevent things such as shared folders from working properly. If you see
    default: shared folder errors, please make sure the guest additions within the
    default: virtual machine match the version of VirtualBox you have installed on
    default: your host and reload your VM.
    default:
    default: Guest Additions Version: 6.1.46
    default: VirtualBox Version: 7.0
==> default: Mounting shared folders...
    default: /vagrant => C:/Users/ZerBr/linux-b2/VagrantF2

PS C:\Users\ZerBr\linux-b2\VagrantF2> vagrant status
Current machine states:

default                   running (virtualbox)

The VM is running. To stop this VM, you can run `vagrant halt` to
shut it down forcefully, or you can run `vagrant suspend` to simply
suspend the virtual machine. In either case, to restart it again,
simply run `vagrant up`.

PS C:\Users\ZerBr\linux-b2\VagrantF2> vagrant ssh
Last login: Thu Jan 18 10:43:16 2024 from 10.0.2.2
```

## 3. Moult VMs


🌞 **Adaptez votre `Vagrantfile`** pour qu'il lance les VMs suivantes (en réutilisant votre box de la partie précédente)

- vous devez utiliser une boucle for dans le `Vagrantfile`
- pas le droit de juste copier coller le même bloc trois fois, une boucle for j'ai dit !

| Name           | IP locale   | Accès internet | RAM |
| -------------- | ----------- | -------------- | --- |
| `node1.tp3.b2` | `10.3.1.11` | Ui             | 1G  |
| `node2.tp3.b2` | `10.3.1.12` | Ui             | 1G  |
| `node3.tp3.b2` | `10.3.1.13` | Ui             | 1G  |

```
PS C:\Users\ZerBr\linux-b2\partie1\Vagrantfile-3A> cat .\Vagrantfile
# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  vms = [
    { name: "node1.tp3.b2", ip: "10.3.1.11", gui: true, ram: 1024 },
    { name: "node2.tp3.b2", ip: "10.3.1.12", gui: true, ram: 1024 },
    { name: "node3.tp3.b2", ip: "10.3.1.13", gui: true, ram: 1024 }
  ]
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "base"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Disable the default share of the current code directory. Doing this
  # provides improved isolation between the vagrant box and your host
  # by making sure your Vagrantfile isn't accessable to the vagrant box.
  # If you use this you may want to enable additional shared subfolders as
  # shown above.
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
  config.ssh.insert_key = false

  vms.each do |vm|
    config.vm.define vm[:name] do |node|
      node.vm.box = "super_box"
      node.vm.hostname = vm[:name]
      node.vm.network "private_network", ip: vm[:ip]
      node.vm.network "forwarded_port", guest: 22, host: 2222, auto_correct: true
      node.vm.provider "virtualbox" do |vb|
        vb.memory = vm[:ram]
        vb.cpus = 2
        vb.gui = vm[:gui]
      end
    end
  end
end
```

📁 **`partie1/Vagrantfile-3A`** dans le dépôt git de rendu

[Vagrantfile-3A](partie1/Vagrantfile-3A)

🌞 **Adaptez votre `Vagrantfile`** pour qu'il lance les VMs suivantes (en réutilisant votre box de la partie précédente)

- l'idéal c'est de déclarer une liste en début de fichier qui contient les données des VMs et de faire un `for` sur cette liste
- à vous de voir, sans boucle `for` et sans liste, juste trois blocs déclarés, ça fonctionne aussi

| Name           | IP locale    | Accès internet | RAM |
| -------------- | ------------ | -------------- | --- |
| `alice.tp3.b2` | `10.3.1.11`  | Ui             | 1G  |
| `bob.tp3.b2`   | `10.3.1.200` | Ui             | 2G  |
| `eve.tp3.b2`   | `10.3.1.57`  | Nan            | 1G  |

```
PS C:\Users\ZerBr\linux-b2\partie1\Vagrantfile-3B> cat .\Vagrantfile
# -*- mode: ruby -*-
# vi: set ft=ruby :

vms = [
    { name: "alice.tp3.b2", ip: "10.3.1.11", gui: true, ram: 1024 },
    { name: "bob.tp3.b2", ip: "10.3.1.200", gui: true, ram: 2048 },
    { name: "eve.tp3.b2", ip: "10.3.1.57", gui: false, ram: 1024 }
]

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "base"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Disable the default share of the current code directory. Doing this
  # provides improved isolation between the vagrant box and your host
  # by making sure your Vagrantfile isn't accessable to the vagrant box.
  # If you use this you may want to enable additional shared subfolders as
  # shown above.
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
  #
  config.ssh.insert_key = false
  #
  vms.each do |vm|
    config.vm.define vm[:name] do |node|
      node.vm.box = "super_box"
      node.vm.hostname = vm[:name]
      node.vm.network "private_network", ip: vm[:ip]
      node.vm.network "forwarded_port", guest: 22, host: 2222, auto_correct: true
      node.vm.provider "virtualbox" do |vb|
        vb.memory = vm[:ram]
        vb.cpus = 2
        vb.gui = vm[:gui]
      end
    end
  end
end
```

📁 **`partie1/Vagrantfile-3B`** dans le dépôt git de rendu

[Vagrantfile-3B](partie1/Vagrantfile-3B)