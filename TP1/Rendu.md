# I. Init

## 3. sudo c pa bo

🌞 **Ajouter votre utilisateur au groupe `docker`**


``` 
 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~  sudo usermod -aG docker martyfiakotron

 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~  su - martyfiakotron
Mot de passe : 

 martyfiakotron@MartyFiakOTron-dellg155510  ~  docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~  alias dk='docker'

 martyfiakotron@MartyFiakOTron-dellg155510  ~  dk ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

```


## 4. Un premier conteneur en vif

🌞 **Lancer un conteneur NGINX**

- avec la commande suivante :

```bash
docker run -d -p 9999:80 nginx
```


``` 
 martyfiakotron@MartyFiakOTron-dellg155510  ~  docker run -d -p 9999:80 nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
af107e978371: Pull complete 
336ba1f05c3e: Pull complete 
8c37d2ff6efa: Pull complete 
51d6357098de: Pull complete 
782f1ecce57d: Pull complete 
5e99d351b073: Pull complete 
7b73345df136: Pull complete 
Digest: sha256:bd30b8d47b230de52431cc71c5cce149b8d5d4c87c204902acf2504435d4b4c9
Status: Downloaded newer image for nginx:latest
a85d1309f74e21de73453428cd5ba80fffe7814b001a6fc6f65160dc163b2d4c
```


🌞 **Visitons**

- vérifier que le conteneur est actif avec une commande qui liste les conteneurs en cours de fonctionnement
- afficher les logs du conteneur
- afficher toutes les informations relatives au conteneur avec une commande `docker inspect`
- afficher le port en écoute sur la VM avec un `sudo ss -lnpt`
- ouvrir le port `9999/tcp` (vu dans le `ss` au dessus normalement) dans le firewall de la VM
- depuis le navigateur de votre PC, visiter le site web sur `http://IP_VM:9999`


``` 
 martyfiakotron@MartyFiakOTron-dellg155510  ~  dk ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                   NAMES
a85d1309f74e   nginx     "/docker-entrypoint.…"   2 minutes ago   Up 2 minutes   0.0.0.0:9999->80/tcp, :::9999->80/tcp   vigilant_raman

 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~  docker logs a8
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/12/21 10:33:22 [notice] 1#1: using the "epoll" event method
2023/12/21 10:33:22 [notice] 1#1: nginx/1.25.3
2023/12/21 10:33:22 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14) 
2023/12/21 10:33:22 [notice] 1#1: OS: Linux 6.1.69-1-lts
2023/12/21 10:33:22 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1073741816:1073741816
2023/12/21 10:33:22 [notice] 1#1: start worker processes
2023/12/21 10:33:22 [notice] 1#1: start worker process 29
2023/12/21 10:33:22 [notice] 1#1: start worker process 30
2023/12/21 10:33:22 [notice] 1#1: start worker process 31
2023/12/21 10:33:22 [notice] 1#1: start worker process 32
2023/12/21 10:33:22 [notice] 1#1: start worker process 33
2023/12/21 10:33:22 [notice] 1#1: start worker process 34
2023/12/21 10:33:22 [notice] 1#1: start worker process 35
2023/12/21 10:33:22 [notice] 1#1: start worker process 36
2023/12/21 10:33:22 [notice] 1#1: start worker process 37
2023/12/21 10:33:22 [notice] 1#1: start worker process 38
2023/12/21 10:33:22 [notice] 1#1: start worker process 39
2023/12/21 10:33:22 [notice] 1#1: start worker process 40

martyfiakotron@MartyFiakOTron-dellg155510  ~  docker inspect a8
[
    {
        "Id": "a85d1309f74e21de73453428cd5ba80fffe7814b001a6fc6f65160dc163b2d4c",
        "Created": "2023-12-21T10:33:22.43936245Z",
        "Path": "/docker-entrypoint.sh",
        "Args": [
            "nginx",
            "-g",
            "daemon off;"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 32401,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2023-12-21T10:33:22.711225006Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:d453dd892d9357f3559b967478ae9cbc417b52de66b53142f6c16c8a275486b9",
        "ResolvConfPath": "/var/lib/docker/containers/a85d1309f74e21de73453428cd5ba80fffe7814b001a6fc6f65160dc163b2d4c/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/a85d1309f74e21de73453428cd5ba80fffe7814b001a6fc6f65160dc163b2d4c/hostname",
        "HostsPath": "/var/lib/docker/containers/a85d1309f74e21de73453428cd5ba80fffe7814b001a6fc6f65160dc163b2d4c/hosts",
        "LogPath": "/var/lib/docker/containers/a85d1309f74e21de73453428cd5ba80fffe7814b001a6fc6f65160dc163b2d4c/a85d1309f74e21de73453428cd5ba80fffe7814b001a6fc6f65160dc163b2d4c-json.log",
        "Name": "/vigilant_raman",
        "RestartCount": 0,
        "Driver": "overlay2",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {
                "80/tcp": [
                    {
                        "HostIp": "",
                        "HostPort": "9999"
                    }
                ]
            },
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "ConsoleSize": [
                24,
                80
            ],
            "CapAdd": null,
            "CapDrop": null,
            "CgroupnsMode": "private",
            "Dns": [],
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "private",
            "Cgroup": "",
            "Links": null,
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": [],
            "BlkioDeviceWriteBps": [],
            "BlkioDeviceReadIOps": [],
            "BlkioDeviceWriteIOps": [],
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DeviceRequests": null,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": null,
            "PidsLimit": null,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0,
            "MaskedPaths": [
                "/proc/asound",
                "/proc/acpi",
                "/proc/kcore",
                "/proc/keys",
                "/proc/latency_stats",
                "/proc/timer_list",
                "/proc/timer_stats",
                "/proc/sched_debug",
                "/proc/scsi",
                "/sys/firmware",
                "/sys/devices/virtual/powercap"
            ],
            "ReadonlyPaths": [
                "/proc/bus",
                "/proc/fs",
                "/proc/irq",
                "/proc/sys",
                "/proc/sysrq-trigger"
            ]
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/a6d1b10830ed94db5f4a69651f8a8217f8a5e69a777fd5ab6677545368951b87-init/diff:/var/lib/docker/overlay2/aa30ec14502329bea702fe02477140a4ecd90c4df7bcb0acee82528063b7002d/diff:/var/lib/docker/overlay2/695435cbc23be193b239acd62cfb88d555258efa86850529cb90ab468a3d48d2/diff:/var/lib/docker/overlay2/56d6e80e49f338fe329d99794d8f630233f1a623b55e7c57bc87fd60e4b67499/diff:/var/lib/docker/overlay2/4205525debb9a1606b2d6cdc1ed78ef9a0f1ed5fe2622e7a725a310ebd332a40/diff:/var/lib/docker/overlay2/4785705d0f8d4911de5410170e9fbe1140d02c4aa0d4f9735441a1eb58e67a8e/diff:/var/lib/docker/overlay2/d7a3639b9a06ee888f98f076b594a56d98524d157e80fc16bf13da354d5fed76/diff:/var/lib/docker/overlay2/bb18a8e89b6030f9271e946d7c7fb2926f471ce5e63e80faaca7f846c0a314c0/diff",
                "MergedDir": "/var/lib/docker/overlay2/a6d1b10830ed94db5f4a69651f8a8217f8a5e69a777fd5ab6677545368951b87/merged",
                "UpperDir": "/var/lib/docker/overlay2/a6d1b10830ed94db5f4a69651f8a8217f8a5e69a777fd5ab6677545368951b87/diff",
                "WorkDir": "/var/lib/docker/overlay2/a6d1b10830ed94db5f4a69651f8a8217f8a5e69a777fd5ab6677545368951b87/work"
            },
            "Name": "overlay2"
        },
        "Mounts": [],
        "Config": {
            "Hostname": "a85d1309f74e",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "ExposedPorts": {
                "80/tcp": {}
            },
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "NGINX_VERSION=1.25.3",
                "NJS_VERSION=0.8.2",
                "PKG_RELEASE=1~bookworm"
            ],
            "Cmd": [
                "nginx",
                "-g",
                "daemon off;"
            ],
            "Image": "nginx",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": [
                "/docker-entrypoint.sh"
            ],
            "OnBuild": null,
            "Labels": {
                "maintainer": "NGINX Docker Maintainers <docker-maint@nginx.com>"
            },
            "StopSignal": "SIGQUIT"
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "50b4bdc5cacd717bd77f72f32bf447babb284708d945dc9204b4ea78f60238a1",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {
                "80/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "9999"
                    },
                    {
                        "HostIp": "::",
                        "HostPort": "9999"
                    }
                ]
            },
            "SandboxKey": "/var/run/docker/netns/50b4bdc5cacd",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "b58b7343109228a4427520ce62d6b906b2619bf525cbdafc202ffbbae5b540b0",
            "Gateway": "172.17.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:02",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "2d100f0e4d320d98d1f9c8192427fc3c39e54c1327ce14a1840f0f08d5ca5eda",
                    "EndpointID": "b58b7343109228a4427520ce62d6b906b2619bf525cbdafc202ffbbae5b540b0",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
            }
        }
    }
]

 martyfiakotron@MartyFiakOTron-dellg155510  ~  sudo ss -lnpt | grep docker
LISTEN 0      4096              0.0.0.0:9999       0.0.0.0:*    users:(("docker-proxy",pid=32362,fd=4))   
LISTEN 0      4096                 [::]:9999          [::]:*    users:(("docker-proxy",pid=32368,fd=4)) 

 martyfiakotron@MartyFiakOTron-dellg155510  ~  sudo firewall-cmd --add-port=9999/tcp --permanent
success
 martyfiakotron@MartyFiakOTron-dellg155510  ~  sudo firewall-cmd --reload
success

 martyfiakotron@MartyFiakOTron-dellg155510  ~  curl http://0.0.0.0:9999/
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

 martyfiakotron@MartyFiakOTron-dellg155510  ~  dk stop a8
a8
 martyfiakotron@MartyFiakOTron-dellg155510  ~  docker run -v /home/martyfiakotron/Documents/:/etc/nginx/conf.d/*.conf -p 9999:80 -d nginx
98336abc062f2258987ee94cdbe3204be5c9e4a48ed8792c2c2332dda979b7e1
```


🌞 **On va ajouter un site Web au conteneur NGINX**

- créez un dossier `nginx`
  - pas n'importe où, c'est ta conf caca, c'est dans ton homedir donc `/home/<TON_USER>/nginx/`
- dedans, deux fichiers : `index.html` (un site nul) `site_nul.conf` (la conf NGINX de notre site nul)
- exemple de `index.html` :

```html
<h1>MEOOOW</h1>
```

- config NGINX minimale pour servir un nouveau site web dans `site_nul.conf` :

```nginx
server {
    listen        8080;

    location / {
        root /var/www/html/index.html;
    }
}
```

- lancez le conteneur avec la commande en dessous, notez que :
  - on partage désormais le port 8080 du conteneur (puisqu'on l'indique dans la conf qu'il doit écouter sur le port 8080)
  - on précise les chemins des fichiers en entier
  - note la syntaxe du `-v` : à gauche le fichier à partager depuis ta machine, à droite l'endroit où le déposer dans le conteneur, séparés par le caractère `:`
  - c'est long putain comme commande

```bash
docker run -d -p 9999:8080 -v /home/<USER>/nginx/index.html:/var/www/html/index.html -v /home/<USER>/nginx/site_nul.conf:/etc/nginx/conf.d/site_nul.conf nginx
```


```
 martyfiakotron@MartyFiakOTron-dellg155510  ~  mkdir /home/martyfiakotron/nginx
mkdir: création du répertoire '/home/martyfiakotron/nginx'

 martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  cat index.html 
<!DOCTYPE html>

<html lang="fr">

    <head>

        <meta charset="utf-8">

        <h1>MEOOOW</h1>

    </head>

    <body>

    </body>

</html>

 martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  cat site_nul.conf 
server {
    listen	8080;

    location / {
    	root /var/www/html;
    }
}

 martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  docker run -d -p 9999:8080 -v /home/martyfiakotron/nginx/index.html:/var/www/html/index.html -v /home/martyfiakotron/nginx/site_nul.conf:/etc/nginx/conf.d/site_nul.conf nginx
f41317e14db4bba5e6fab2b0871ffc39bb63d9b2ce31604a030c39a5908eb9fd
```


🌞 **Visitons**

- vérifier que le conteneur est actif
- aucun port firewall à ouvrir : on écoute toujours port 9999 sur la machine hôte (la VM)
- visiter le site web depuis votre PC


```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  dk ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                               NAMES
f41317e14db4   nginx     "/docker-entrypoint.…"   2 minutes ago   Up 2 minutes   80/tcp, 0.0.0.0:9999->8080/tcp, :::9999->8080/tcp   dazzling_swirles

 martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  curl 0.0.0.0:9999
<!DOCTYPE html>

<html lang="fr">

    <head>

        <meta charset="utf-8">

        <h1>MEOOOW</h1>

    </head>

    <body>

    </body>

</html>
```


## 5. Un deuxième conteneur en vif

🌞 **Lance un conteneur Python, avec un shell**

- il faut indiquer au conteneur qu'on veut lancer un shell
- un shell c'est "interactif" : on saisit des trucs (input) et ça nous affiche des trucs (output)
  - il faut le préciser dans la commande `docker run` avec `-it`
- ça donne donc :

```bash
# on lance un conteneur "python" de manière interactive
# et on demande à ce conteneur d'exécuter la commande "bash" au démarrage
docker run -it python bash
```

```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  docker run -it python bash
Unable to find image 'python:latest' locally
latest: Pulling from library/python
bc0734b949dc: Pull complete 
b5de22c0f5cd: Pull complete 
917ee5330e73: Pull complete 
b43bd898d5fb: Pull complete 
7fad4bffde24: Pull complete 
d685eb68699f: Pull complete 
107007f161d0: Pull complete 
02b85463d724: Pull complete 
Digest: sha256:3733015cdd1bd7d9a0b9fe21a925b608de82131aa4f3d397e465a1fcb545d36f
Status: Downloaded newer image for python:latest
```


🌞 **Installe des libs Python**

- une fois que vous avez lancé le conteneur, et que vous êtes dedans avec `bash`
- installez deux libs, elles ont été choisies complètement au hasard (avec la commande `pip install`):
  - `aiohttp`
  - `aioconsole`
- tapez la commande `python` pour ouvrir un interpréteur Python
- taper la ligne `import aiohttp` pour vérifier que vous avez bien téléchargé la lib


```
root@fe1196fb7e52:/# pip install aiohttp
Collecting aiohttp
  Obtaining dependency information for aiohttp from https://files.pythonhosted.org/packages/75/5f/90a2869ad3d1fb9bd984bfc1b02d8b19e381467b238bd3668a09faa69df5/aiohttp-3.9.1-cp312-cp312-manylinux_2_17_x86_64.manylinux2014_x86_64.whl.metadata
  Downloading aiohttp-3.9.1-cp312-cp312-manylinux_2_17_x86_64.manylinux2014_x86_64.whl.metadata (7.4 kB)
Collecting attrs>=17.3.0 (from aiohttp)
  Downloading attrs-23.1.0-py3-none-any.whl (61 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 61.2/61.2 kB 2.9 MB/s eta 0:00:00
Collecting multidict<7.0,>=4.5 (from aiohttp)
  Downloading multidict-6.0.4.tar.gz (51 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 51.3/51.3 kB 6.3 MB/s eta 0:00:00
  Installing build dependencies ... done
  Getting requirements to build wheel ... done
  Installing backend dependencies ... done
  Preparing metadata (pyproject.toml) ... done
Collecting yarl<2.0,>=1.0 (from aiohttp)
  Obtaining dependency information for yarl<2.0,>=1.0 from https://files.pythonhosted.org/packages/28/1c/bdb3411467b805737dd2720b85fd082e49f59bf0cc12dc1dfcc80ab3d274/yarl-1.9.4-cp312-cp312-manylinux_2_17_x86_64.manylinux2014_x86_64.whl.metadata
  Downloading yarl-1.9.4-cp312-cp312-manylinux_2_17_x86_64.manylinux2014_x86_64.whl.metadata (31 kB)
Collecting frozenlist>=1.1.1 (from aiohttp)
  Obtaining dependency information for frozenlist>=1.1.1 from https://files.pythonhosted.org/packages/0b/f2/b8158a0f06faefec33f4dff6345a575c18095a44e52d4f10c678c137d0e0/frozenlist-1.4.1-cp312-cp312-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_17_x86_64.manylinux2014_x86_64.whl.metadata
  Downloading frozenlist-1.4.1-cp312-cp312-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_17_x86_64.manylinux2014_x86_64.whl.metadata (12 kB)
Collecting aiosignal>=1.1.2 (from aiohttp)
  Downloading aiosignal-1.3.1-py3-none-any.whl (7.6 kB)
Collecting idna>=2.0 (from yarl<2.0,>=1.0->aiohttp)
  Obtaining dependency information for idna>=2.0 from https://files.pythonhosted.org/packages/c2/e7/a82b05cf63a603df6e68d59ae6a68bf5064484a0718ea5033660af4b54a9/idna-3.6-py3-none-any.whl.metadata
  Downloading idna-3.6-py3-none-any.whl.metadata (9.9 kB)
Downloading aiohttp-3.9.1-cp312-cp312-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (1.3 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 1.3/1.3 MB 6.3 MB/s eta 0:00:00
Downloading frozenlist-1.4.1-cp312-cp312-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_17_x86_64.manylinux2014_x86_64.whl (281 kB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 281.5/281.5 kB 10.2 MB/s eta 0:00:00
Downloading yarl-1.9.4-cp312-cp312-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (322 kB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 322.4/322.4 kB 22.0 MB/s eta 0:00:00
Downloading idna-3.6-py3-none-any.whl (61 kB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 61.6/61.6 kB 6.5 MB/s eta 0:00:00
Building wheels for collected packages: multidict
  Building wheel for multidict (pyproject.toml) ... done
  Created wheel for multidict: filename=multidict-6.0.4-cp312-cp312-linux_x86_64.whl size=114916 sha256=5e80495b64f2e0c539ea82e0c5f3ad282ebc0497dde0878de649fe59060a5923
  Stored in directory: /root/.cache/pip/wheels/f6/d8/ff/3c14a64b8f2ab1aa94ba2888f5a988be6ab446ec5c8d1a82da
Successfully built multidict
Installing collected packages: multidict, idna, frozenlist, attrs, yarl, aiosignal, aiohttp
Successfully installed aiohttp-3.9.1 aiosignal-1.3.1 attrs-23.1.0 frozenlist-1.4.1 idna-3.6 multidict-6.0.4 yarl-1.9.4
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv

[notice] A new release of pip is available: 23.2.1 -> 23.3.2
[notice] To update, run: pip install --upgrade pip
root@fe1196fb7e52:/# pip install aioconsole
Collecting aioconsole
  Obtaining dependency information for aioconsole from https://files.pythonhosted.org/packages/f7/39/b392dc1a8bb58342deacc1ed2b00edf88fd357e6fdf76cc6c8046825f84f/aioconsole-0.7.0-py3-none-any.whl.metadata
  Downloading aioconsole-0.7.0-py3-none-any.whl.metadata (5.3 kB)
Downloading aioconsole-0.7.0-py3-none-any.whl (30 kB)
Installing collected packages: aioconsole
Successfully installed aioconsole-0.7.0
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv

[notice] A new release of pip is available: 23.2.1 -> 23.3.2
[notice] To update, run: pip install --upgrade pip

root@fe1196fb7e52:/# python
Python 3.12.1 (main, Dec 19 2023, 20:14:15) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import aiohttp
>>> 

(
root@fe1196fb7e52:/# ls
bin   dev  home  lib32	libx32	mnt  proc  run	 srv  tmp  var
boot  etc  lib	 lib64	media	opt  root  sbin  sys  usr
root@fe1196fb7e52:/# ls /var
backups  cache	lib  local  lock  log  mail  opt  run  spool  tmp
root@fe1196fb7e52:/# ls /usr
bin  games  include  lib  lib32  lib64	libexec  libx32  local	sbin  share  src
)
```

# II. Images

## 1. Images publiques

🌞 **Récupérez des images**

- avec la commande `docker pull`
- récupérez :
  - l'image `python` officielle en version 3.11 (`python:3.11` pour la dernière version)
  - l'image `mysql` officielle en version 5.7
  - l'image `wordpress` officielle en dernière version
    - c'est le tag `:latest` pour récupérer la dernière version
    - si aucun tag n'est précisé, `:latest` est automatiquement ajouté
  - l'image `linuxserver/wikijs` en dernière version
    - ce n'est pas une image officielle car elle est hébergée par l'utilisateur `linuxserver` contrairement aux 3 précédentes
    - on doit donc avoir un moins haut niveau de confiance en cette image
- listez les images que vous avez sur la machine avec une commande `docker`


```
 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  docker image pull python:3.11
3.11: Pulling from library/python
bc0734b949dc: Already exists 
b5de22c0f5cd: Already exists 
917ee5330e73: Already exists 
b43bd898d5fb: Already exists 
7fad4bffde24: Already exists 
1f68ce6a3e62: Pull complete 
e27d998f416b: Pull complete 
fefdcd9854bf: Pull complete 
Digest: sha256:4e5e9b05dda9cf699084f20bb1d3463234446387fa0f7a45d90689c48e204c83
Status: Downloaded newer image for python:3.11
docker.io/library/python:3.11

 martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  docker image pull mysql:5.7
5.7: Pulling from library/mysql
20e4dcae4c69: Pull complete 
1c56c3d4ce74: Pull complete 
e9f03a1c24ce: Pull complete 
68c3898c2015: Pull complete 
6b95a940e7b6: Pull complete 
90986bb8de6e: Pull complete 
ae71319cb779: Pull complete 
ffc89e9dfd88: Pull complete 
43d05e938198: Pull complete 
064b2d298fba: Pull complete 
df9a4d85569b: Pull complete 
Digest: sha256:4bc6bc963e6d8443453676cae56536f4b8156d78bae03c0145cbe47c2aad73bb
Status: Downloaded newer image for mysql:5.7
docker.io/library/mysql:5.7

 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  docker image pull wordpress
Using default tag: latest
latest: Pulling from library/wordpress
af107e978371: Already exists 
6480d4ad61d2: Pull complete 
95f5176ece8b: Pull complete 
0ebe7ec824ca: Pull complete 
673e01769ec9: Pull complete 
74f0c50b3097: Pull complete 
1a19a72eb529: Pull complete 
50436df89cfb: Pull complete 
8b616b90f7e6: Pull complete 
df9d2e4043f8: Pull complete 
d6236f3e94a1: Pull complete 
59fa8b76a6b3: Pull complete 
99eb3419cf60: Pull complete 
22f5c20b545d: Pull complete 
1f0d2c1603d0: Pull complete 
4624824acfea: Pull complete 
79c3af11cab5: Pull complete 
e8d8239610fb: Pull complete 
a1ff013e1d94: Pull complete 
31076364071c: Pull complete 
87728bbad961: Pull complete 
Digest: sha256:ffabdfe91eefc08f9675fe0e0073b2ebffa8a62264358820bcf7319b6dc09611
Status: Downloaded newer image for wordpress:latest
docker.io/library/wordpress:latest

 martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  docker image pull linuxserver/wikijs
Using default tag: latest
latest: Pulling from linuxserver/wikijs
8b16ab80b9bd: Pull complete 
07a0e16f7be1: Pull complete 
145cda5894de: Pull complete 
1a16fa4f6192: Pull complete 
84d558be1106: Pull complete 
4573be43bb06: Pull complete 
20b23561c7ea: Pull complete 
Digest: sha256:131d247ab257cc3de56232b75917d6f4e24e07c461c9481b0e7072ae8eba3071
Status: Downloaded newer image for linuxserver/wikijs:latest
docker.io/linuxserver/wikijs:latest
```


🌞 **Lancez un conteneur à partir de l'image Python**

- lancez un terminal `bash` ou `sh`
- vérifiez que la commande `python` est installée dans la bonne version


```
 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~/nginx  sh
sh-5.2$ python -V
Python 3.11.6
```


## 2. Construire une image

Pour construire une image il faut :

- créer un fichier `Dockerfile`
- exécuter une commande `docker build` pour produire une image à partir du `Dockerfile`

🌞 **Ecrire un Dockerfile pour une image qui héberge une application Python**

- l'image doit contenir
  - une base debian (un `FROM`)
  - l'installation de Python (un `RUN` qui lance un `apt install`)
    - il faudra forcément `apt update` avant
    - en effet, le conteneur a été allégé au point d'enlever la liste locale des paquets dispos
    - donc nécessaire d'update avant de install quoique ce soit
  - l'installation de la librairie Python `emoji` (un `RUN` qui lance un `pip install`)
  - ajout de l'application (un `COPY`)
  - le lancement de l'application (un `ENTRYPOINT`)
- le code de l'application :

```python
import emoji

print(emoji.emojize("Cet exemple d'application est vraiment naze :thumbs_down:"))
```

- pour faire ça, créez un dossier `python_app_build`
  - pas n'importe où, c'est ton Dockerfile, ton caca, c'est dans ton homedir donc `/home/<USER>/python_app_build`
  - dedans, tu mets le code dans un fichier `app.py`
  - tu mets aussi `le Dockerfile` dedans


```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/python_app_build  cat Dockerfile 
FROM debian

RUN apt-get update && \
    apt-get install -y python3 python3-pip

RUN pip install emoji

COPY app.py /home/user/python_app_build/app.py

WORKDIR /home/user/python_app_build

ENTRYPOINT ["python3","app.py"] 

 martyfiakotron@MartyFiakOTron-dellg155510  ~/python_app_build  cat app.py 
import emoji

print(emoji.emojize("Cet exemple d'application est vraiment naze :thumbs_down:"))
```


🌞 **Build l'image**

- déplace-toi dans ton répertoire de build `cd python_app_build`
- `docker build . -t python_app:version_de_ouf`
  - le `.` indique le chemin vers le répertoire de build (`.` c'est le dossier actuel)
  - `-t python_app:version_de_ouf` permet de préciser un nom d'image (ou *tag*)
- une fois le build terminé, constater que l'image est dispo avec une commande `docker`

```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/python_app_build  docker build . -t python_app:version_de_ouf
 [...]
Successfully built 0770e1ed44e0
Successfully tagged python_app:version_de_ouf
```

🌞 **Lancer l'image**

- lance l'image avec `docker run` :

```bash
docker run python_app:version_de_ouf
```


```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/python_app_build  docker run python_app:version_de_ouf
Cet exemple d'application est vraiment naze 👎

```


# III. Docker compose


🌞 **Créez un fichier `docker-compose.yml`**

- dans un nouveau dossier dédié `/home/<USER>/compose_test`
- le contenu est le suivant :

```yml
version: "3"

services:
  conteneur_nul:
    image: debian
    entrypoint: sleep 9999
  conteneur_flopesque:
    image: debian
    entrypoint: sleep 9999
```

Ce fichier est parfaitement équivalent à l'enchaînement de commandes suivantes (*ne les faites pas hein*, c'est juste pour expliquer) :

```bash
$ docker network create compose_test
$ docker run --name conteneur_nul --network compose_test debian sleep 9999
$ docker run --name conteneur_flopesque --network compose_test debian sleep 9999
```


```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/compose_test  cat docker-compose.yml 
version: "3"

services:
  conteneur_nul:
    image: debian
    entrypoint: sleep 9999
  conteneur_flopesque:
    image: debian
    entrypoint: sleep 9999
```

🌞 **Lancez les deux conteneurs** avec `docker compose`

- déplacez-vous dans le dossier `compose_test` qui contient le fichier `docker-compose.yml`
- go exécuter `docker compose up -d`


```
 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~/compose_test  docker compose up -d
[+] Running 3/3
 ✔ Network compose_test_default                  Created                                                                                                                                                                   0.2s 
 ✔ Container compose_test-conteneur_flopesque-1  Started                                                                                                                                                                   0.1s 
 ✔ Container compose_test-conteneur_nul-1        Started  
```


🌞 **Vérifier que les deux conteneurs tournent**

- toujours avec une commande `docker`
- tu peux aussi use des trucs comme `docker compose ps` ou `docker compose top` qui sont cools dukoo
  - `docker compose --help` pour voir les bails

```
   martyfiakotron@MartyFiakOTron-dellg155510  ~/compose_test  docker compose ps
NAME                                 IMAGE     COMMAND        SERVICE               CREATED          STATUS          PORTS
compose_test-conteneur_flopesque-1   debian    "sleep 9999"   conteneur_flopesque   32 minutes ago   Up 32 minutes   
compose_test-conteneur_nul-1         debian    "sleep 9999"   conteneur_nul         32 minutes ago   Up 32 minutes 

 martyfiakotron@MartyFiakOTron-dellg155510  ~/compose_test  docker compose top
compose_test-conteneur_flopesque-1
UID    PID     PPID    C    STIME   TTY   TIME       CMD
root   27820   27792   0    11:52   ?     00:00:00   sleep 9999   

compose_test-conteneur_nul-1
UID    PID     PPID    C    STIME   TTY   TIME       CMD
root   27798   27763   0    11:52   ?     00:00:00   sleep 9999  
```

🌞 **Pop un shell dans le conteneur `conteneur_nul`**

- référez-vous au mémo Docker
- effectuez un `ping conteneur_flopesque` (ouais ouais, avec ce nom là)
  - un conteneur est aussi léger que possible, aucun programme/fichier superflu : t'auras pas la commande `ping` !
  - il faudra installer un paquet qui fournit la commande `ping` pour pouvoir tester
  - juste pour te faire remarquer que les conteneurs ont pas besoin de connaître leurs IP : les noms fonctionnent

```
 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~/compose_test  docker ps
CONTAINER ID   IMAGE     COMMAND        CREATED      STATUS         PORTS     NAMES
41e57f456251   debian    "sleep 9999"   6 days ago   Up 2 minutes             compose_test-conteneur_flopesque-1
9c0449fde0b4   debian    "sleep 9999"   6 days ago   Up 2 minutes             compose_test-conteneur_nul-1

 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~/compose_test  docker exec -it 9c bash
root@9c0449fde0b4:/# ping conteneur_flopesque
bash: ping: command not found

root@9c0449fde0b4:/# apt update                     
Get:1 http://deb.debian.org/debian bookworm InRelease [151 kB]
Get:2 http://deb.debian.org/debian bookworm-updates InRelease [52.1 kB]
Get:3 http://deb.debian.org/debian-security bookworm-security InRelease [48.0 kB]
Get:4 http://deb.debian.org/debian bookworm/main amd64 Packages [8787 kB]
Get:5 http://deb.debian.org/debian bookworm-updates/main amd64 Packages [11.5 kB]                           
Get:6 http://deb.debian.org/debian-security bookworm-security/main amd64 Packages [132 kB]                  
Fetched 9182 kB in 25s (370 kB/s)                                                                           
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.

root@9c0449fde0b4:/# apt-get install iputils-ping -y
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  libcap2-bin libpam-cap
The following NEW packages will be installed:
  iputils-ping libcap2-bin libpam-cap
0 upgraded, 3 newly installed, 0 to remove and 0 not upgraded.
Need to get 96.2 kB of archives.
After this operation, 311 kB of additional disk space will be used.
Get:1 http://deb.debian.org/debian bookworm/main amd64 libcap2-bin amd64 1:2.66-4 [34.7 kB]
Get:2 http://deb.debian.org/debian bookworm/main amd64 iputils-ping amd64 3:20221126-1 [47.1 kB]
Get:3 http://deb.debian.org/debian bookworm/main amd64 libpam-cap amd64 1:2.66-4 [14.5 kB]
Fetched 96.2 kB in 0s (415 kB/s)     
debconf: delaying package configuration, since apt-utils is not installed
Selecting previously unselected package libcap2-bin.
(Reading database ... 6098 files and directories currently installed.)
Preparing to unpack .../libcap2-bin_1%3a2.66-4_amd64.deb ...
Unpacking libcap2-bin (1:2.66-4) ...
Selecting previously unselected package iputils-ping.
Preparing to unpack .../iputils-ping_3%3a20221126-1_amd64.deb ...
Unpacking iputils-ping (3:20221126-1) ...
Selecting previously unselected package libpam-cap:amd64.
Preparing to unpack .../libpam-cap_1%3a2.66-4_amd64.deb ...
Unpacking libpam-cap:amd64 (1:2.66-4) ...
Setting up libcap2-bin (1:2.66-4) ...
Setting up libpam-cap:amd64 (1:2.66-4) ...
debconf: unable to initialize frontend: Dialog
debconf: (No usable dialog-like program is installed, so the dialog based frontend cannot be used. at /usr/share/perl5/Debconf/FrontEnd/Dialog.pm line 78.)
debconf: falling back to frontend: Readline
debconf: unable to initialize frontend: Readline
debconf: (Can't locate Term/ReadLine.pm in @INC (you may need to install the Term::ReadLine module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.36.0 /usr/local/share/perl/5.36.0 /usr/lib/x86_64-linux-gnu/perl5/5.36 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl-base /usr/lib/x86_64-linux-gnu/perl/5.36 /usr/share/perl/5.36 /usr/local/lib/site_perl) at /usr/share/perl5/Debconf/FrontEnd/Readline.pm line 7.)
debconf: falling back to frontend: Teletype
Setting up iputils-ping (3:20221126-1) ...

root@9c0449fde0b4:/# ping conteneur_flopesque
PING conteneur_flopesque (172.18.0.3) 56(84) bytes of data.
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.3): icmp_seq=1 ttl=64 time=0.200 ms
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.3): icmp_seq=2 ttl=64 time=0.124 ms
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.3): icmp_seq=3 ttl=64 time=0.148 ms
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.3): icmp_seq=4 ttl=64 time=0.126 ms
^C
--- conteneur_flopesque ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3022ms
rtt min/avg/max/mdev = 0.124/0.149/0.200/0.030 ms
```
