# TP4 : Vers une maîtrise des OS Linux

## Sommaire

- [TP4 : Vers une maîtrise des OS Linux](#tp4--vers-une-maîtrise-des-os-linux)
  - [Sommaire](#sommaire)
- [I. Partitionnement](#i-partitionnement)
  - [1. LVM dès l'installation](#1-lvm-dès-linstallation)
  - [2. Scénario remplissage de partition](#2-scénario-remplissage-de-partition)
- [II. Gestion de users](#ii-gestion-de-users)
- [III. Gestion du temps](#iii-gestion-du-temps)

# I. Partitionnement

## 1. LVM dès l'installation

🌞 **Faites une install manuelle de Rocky Linux**

```
[dora@localhost ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda           8:0    0 40.3G  0 disk 
├─sda1        8:1    0   21G  0 part 
│ ├─rl-root 253:0    0   10G  0 lvm  /
│ ├─rl-swap 253:1    0    1G  0 lvm  [SWAP]
│ ├─rl-var  253:2    0    5G  0 lvm  /var
│ └─rl-home 253:3    0    5G  0 lvm  /home
└─sda2        8:2    0    1G  0 part /boot
sr0          11:0    1 1024M  0 rom  


[dora@localhost ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             4.0M     0  4.0M   0% /dev
tmpfs                882M     0  882M   0% /dev/shm
tmpfs                353M  5.0M  348M   2% /run
/dev/mapper/rl-root  9.8G  959M  8.3G  11% /
/dev/sda2            974M  183M  725M  21% /boot
/dev/mapper/rl-home  4.9G   40K  4.6G   1% /home
/dev/mapper/rl-var   4.9G   81M  4.5G   2% /var
tmpfs                177M     0  177M   0% /run/user/1000


[dora@localhost ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda1  rl lvm2 a--  21.00g 4.00m
[dora@localhost ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda1
  VG Name               rl
  PV Size               <21.01 GiB / not usable 4.00 MiB
  Allocatable           yes 
  PE Size               4.00 MiB
  Total PE              5377
  Free PE               1
  Allocated PE          5376
  PV UUID               nNdDEd-3u1V-34xZ-1aeu-2ScP-zGmX-SEGj8t

  [dora@localhost ~]$ sudo vgs
  VG #PV #LV #SN Attr   VSize  VFree
  rl   1   4   0 wz--n- 21.00g 4.00m
[dora@localhost ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               rl
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  5
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                4
  Open LV               4
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               21.00 GiB
  PE Size               4.00 MiB
  Total PE              5377
  Alloc PE / Size       5376 / 21.00 GiB
  Free  PE / Size       1 / 4.00 MiB
  VG UUID               ja47fk-drka-CZL8-ylht-oVJj-QNVB-eYklny

[dora@localhost ~]$ sudo lvs
  LV   VG Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home rl -wi-ao----  5.00g                                                    
  root rl -wi-ao---- 10.00g                                                    
  swap rl -wi-ao----  1.00g                                                    
  var  rl -wi-ao----  5.00g                                                    
[dora@localhost ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/rl/root
  LV Name                root
  VG Name                rl
  LV UUID                kCOlSd-E0kD-1OS6-htlM-WxPF-ywxZ-aP922u
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2024-01-25 11:18:28 +0100
  LV Status              available
  # open                 1
  LV Size                10.00 GiB
  Current LE             2560
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0
   
  --- Logical volume ---
  LV Path                /dev/rl/var
  LV Name                var
  VG Name                rl
  LV UUID                tpwXEi-dHxp-GPoc-VFXf-q90z-trTn-cylzHE
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2024-01-25 11:18:29 +0100
  LV Status              available
  # open                 1
  LV Size                5.00 GiB
  Current LE             1280
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:2
   
  --- Logical volume ---
  LV Path                /dev/rl/home
  LV Name                home
  VG Name                rl
  LV UUID                37qNmJ-z3Zr-pkPr-Cq9Z-MWjU-4I75-PLyDhw
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2024-01-25 11:18:30 +0100
  LV Status              available
  # open                 1
  LV Size                5.00 GiB
  Current LE             1280
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:3
   
  --- Logical volume ---
  LV Path                /dev/rl/swap
  LV Name                swap
  VG Name                rl
  LV UUID                akoGHN-nqTH-kbmC-AB19-YD2Y-xgzU-yIeaHE
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2024-01-25 11:18:31 +0100
  LV Status              available
  # open                 2
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:1
```

## 2. Scénario remplissage de partition

🌞 **Remplissez votre partition `/home`**

```
[dora@localhost ~]$ dd if=/dev/zero of=/home/dora/bigfile bs=4M count=5000
dd: error writing '/home/dora/bigfile': No space left on device
1235+0 records in
1234+0 records out
5179559936 bytes (5.2 GB, 4.8 GiB) copied, 17.3443 s, 299 MB/s
```

🌞 **Constater que la partition est pleine**

- avec un `df -h`

```
[dora@localhost ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             4.0M     0  4.0M   0% /dev
tmpfs                882M     0  882M   0% /dev/shm
tmpfs                353M  5.0M  348M   2% /run
/dev/mapper/rl-root  9.8G  1.1G  8.2G  12% /
/dev/sda2            974M  234M  674M  26% /boot
/dev/mapper/rl-home  4.9G  4.9G     0 100% /home
/dev/mapper/rl-var   4.9G  143M  4.5G   4% /var
tmpfs                177M     0  177M   0% /run/user/1000
```

🌞 **Agrandir la partition**

- avec des commandes LVM il faut agrandir le logical volume
- ensuite il faudra indiquer au système de fichier ext4 que la partition a été agrandie
- prouvez avec un `df -h` que vous avez récupéré de l'espace en plus

```
[dora@localhost ~]$ sudo fdisk /dev/sda

Welcome to fdisk (util-linux 2.37.4).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

This disk is currently in use - repartitioning is probably a bad idea.
It's recommended to umount all file systems, and swapoff all swap
partitions on this disk.


Command (m for help): help
h: unknown command

Command (m for help): 


Command (m for help): m

Help:

  DOS (MBR)
   a   toggle a bootable flag
   b   edit nested BSD disklabel
   c   toggle the dos compatibility flag

  Generic
   d   delete a partition
   F   list free unpartitioned space
   l   list known partition types
   n   add a new partition
   p   print the partition table
   t   change a partition type
   v   verify the partition table
   i   print information about a partition

  Misc
   m   print this menu
   u   change display/entry units
   x   extra functionality (experts only)

  Script
   I   load disk layout from sfdisk script file
   O   dump disk layout to sfdisk script file

  Save & Exit
   w   write table to disk and exit
   q   quit without saving changes

  Create a new label
   g   create a new empty GPT partition table
   G   create a new empty SGI (IRIX) partition table
   o   create a new empty DOS partition table
   s   create a new empty Sun partition table


Command (m for help): n
Partition type
   p   primary (2 primary, 0 extended, 2 free)
   e   extended (container for logical partitions)
Select (default p): 

Using default response p.
Partition number (3,4, default 3): 
First sector (46155776-84585087, default 46155776): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (46155776-84585087, default 84585087): 

Created a new partition 3 of type 'Linux' and of size 18.3 GiB.

Command (m for help): w
The partition table has been altered.
Syncing disks.


[dora@localhost ~]$ sudo fdisk -l


Disk /dev/sda: 40.33 GiB, 43307565056 bytes, 84585088 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xb57ae04c

Device     Boot    Start      End  Sectors Size Id Type
/dev/sda1           2048 44058623 44056576  21G 8e Linux LVM
/dev/sda2  *    44058624 46155775  2097152   1G 83 Linux


Disk /dev/mapper/rl-root: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/rl-swap: 1 GiB, 1073741824 bytes, 2097152 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/rl-var: 5 GiB, 5368709120 bytes, 10485760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/rl-home: 5 GiB, 5372903424 bytes, 10493952 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
[dora@localhost ~]$ 

[dora@localhost ~]$ sudo pvcreate /dev/sda3
  Physical volume "/dev/sda3" successfully created.

[dora@localhost ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree 
  /dev/sda1  rl lvm2 a--  21.00g     0 
  /dev/sda3  rl lvm2 a--  18.32g 18.32g


[dora@localhost ~]$ sudo vgextend rl /dev/sda3
  Volume group "rl" successfully extended

[dora@localhost ~]$ sudo lvextend -l +100%FREE /dev/rl/home 
  Size of logical volume rl/home changed from 5.00 GiB (1281 extents) to 23.32 GiB (5971 extents).
  Logical volume rl/home successfully resized.

[dora@localhost ~]$ sudo resize2fs /dev/rl/home 
resize2fs 1.46.5 (30-Dec-2021)
Filesystem at /dev/rl/home is mounted on /home; on-line resizing required
old_desc_blocks = 1, new_desc_blocks = 3
The filesystem on /dev/rl/home is now 6114304 (4k) blocks long.

[dora@localhost ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             4.0M     0  4.0M   0% /dev
tmpfs                882M     0  882M   0% /dev/shm
tmpfs                353M  5.0M  348M   2% /run
/dev/mapper/rl-root  9.8G  1.1G  8.2G  12% /
/dev/sda2            974M  234M  674M  26% /boot
/dev/mapper/rl-home   23G  4.9G   18G  23% /home
/dev/mapper/rl-var   4.9G  143M  4.5G   4% /var
tmpfs                177M     0  177M   0% /run/user/1000
```

🌞 **Remplissez votre partition `/home`**

- on va simuler encore avec un truc bourrin :

```
sudo dd if=/dev/zero of=/home/dora/bigfile bs=4M count=5000
[sudo] password for dora: 

5000+0 records in
5000+0 records out
20971520000 bytes (21 GB, 20 GiB) copied, 66.9883 s, 313 MB/s
```

🌞 **Utiliser ce nouveau disque pour étendre la partition `/home` de 40G**

- dans l'ordre il faut :
- indiquer à LVM qu'il y a un nouveau PV dispo
- ajouter ce nouveau PV au VG existant
- étendre le LV existant pour récupérer le nouvel espace dispo au sein du VG
- indiquer au système de fichier ext4 que la partition a été agrandie
- prouvez avec un `df -h` que vous avez récupéré de l'espace en plus

``` 
[dora@localhost ~]$ sudo fdisk /dev/sdb
[sudo] password for dora: 

Welcome to fdisk (util-linux 2.37.4).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table.
Created a new DOS disklabel with disk identifier 0x25948063.

Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): 

Using default response p.
Partition number (1-4, default 1): 
First sector (2048-83886079, default 2048): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-83886079, default 83886079): 

Created a new partition 1 of type 'Linux' and of size 40 GiB.

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.

[dora@localhost ~]$ sudo fdisk -l


Disk /dev/sda: 40.33 GiB, 43307565056 bytes, 84585088 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xe03a87bb

Device     Boot    Start      End  Sectors  Size Id Type
/dev/sda1           2048 44058623 44056576   21G 8e Linux LVM
/dev/sda2  *    44058624 46155775  2097152    1G 83 Linux
/dev/sda3       46155776 84585087 38429312 18.3G 83 Linux


Disk /dev/sdb: 40 GiB, 42949672960 bytes, 83886080 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x25948063

Device     Boot Start      End  Sectors Size Id Type
/dev/sdb1        2048 83886079 83884032  40G 83 Linux


Disk /dev/mapper/rl-root: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/rl-swap: 1 GiB, 1073741824 bytes, 2097152 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/rl-home: 23.32 GiB, 25044189184 bytes, 48914432 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/rl-var: 5 GiB, 5368709120 bytes, 10485760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
[dora@localhost ~]$ sudo pvcreate /dev/sdb
  Cannot use /dev/sdb: device is partitioned
[dora@localhost ~]$ sudo pvcreate /dev/sd
sda   sda1  sda2  sda3  sdb   sdb1  
[dora@localhost ~]$ sudo pvcreate /dev/sdb
  Cannot use /dev/sdb: device is partitioned
[dora@localhost ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             4.0M     0  4.0M   0% /dev
tmpfs                1.2G     0  1.2G   0% /dev/shm
tmpfs                466M  6.4M  459M   2% /run
/dev/mapper/rl-root  9.8G  1.1G  8.2G  12% /
/dev/sda2            974M  264M  643M  30% /boot
/dev/mapper/rl-home   23G   20G  2.4G  90% /home
/dev/mapper/rl-var   4.9G  143M  4.5G   4% /var
tmpfs                233M     0  233M   0% /run/user/1000
[dora@localhost ~]$ sudo pvcreate /dev/sdb
  Cannot use /dev/sdb: device is partitioned
[dora@localhost ~]$ sudo pvcreate /dev/sdb
sdb   sdb1  
[dora@localhost ~]$ sudo pvcreate /dev/sdb
sdb   sdb1  
[dora@localhost ~]$ sudo pvcreate /dev/sdb1
  Physical volume "/dev/sdb1" successfully created.
[dora@localhost ~]$ sudo vgextend rl /dev/sdb1
  Volume group "rl" successfully extended
[dora@localhost ~]$ sudo lvextend -l +100%FREE /dev/rl/home 
  Size of logical volume rl/home changed from 23.32 GiB (5971 extents) to 63.32 GiB (16210 extents).
  Logical volume rl/home successfully resized.
[dora@localhost ~]$ sudo resize2fs /dev/rl/home 
resize2fs 1.46.5 (30-Dec-2021)
Filesystem at /dev/rl/home is mounted on /home; on-line resizing required
old_desc_blocks = 3, new_desc_blocks = 8
The filesystem on /dev/rl/home is now 16599040 (4k) blocks long.

[dora@localhost ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             4.0M     0  4.0M   0% /dev
tmpfs                1.2G     0  1.2G   0% /dev/shm
tmpfs                466M  6.4M  459M   2% /run
/dev/mapper/rl-root  9.8G  1.1G  8.2G  12% /
/dev/sda2            974M  264M  643M  30% /boot
/dev/mapper/rl-home   63G   20G   41G  33% /home
/dev/mapper/rl-var   4.9G  143M  4.5G   4% /var
tmpfs                233M     0  233M   0% /run/user/1000
```

# II. Gestion de users


🌞 **Gestion basique de users**

- créez des users en respactant le tableau suivant :

| Name    | Groupe primaire | Groupes secondaires | Password | Homedir         | Shell              |
| ------- | --------------- | ------------------- | -------- | --------------- | ------------------ |
| alice   | alice           | admins              | toto     | `/home/alice`   | `/bin/bash`        |
| bob     | bob             | admins              | toto     | `/home/bob`     | `/bin/bash`        |
| charlie | charlie         | admins              | toto     | `/home/charlie` | `/bin/bash`        |
| eve     | eve             | N/A                 | toto     | `/home/eve`     | `/bin/bash`        |
| backup  | backup          | N/A                 | toto     | `/var/backup`   | `/usr/bin/nologin` |

- prouvez que tout ça est ok avec juste un `cat` du fichier adapté (y'a pas le password dedans bien sûr)

```
[dora@localhost ~]$ sudo groupadd alice
[dora@localhost ~]$ sudo groupadd admins
[dora@localhost ~]$ sudo useradd -m -g alice -G admins -s /bin/bash alice
[dora@localhost ~]$ sudo passwd alice
Changing password for user alice.
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.
[dora@localhost ~]$ sudo cat /etc/group | grep alice
alice:x:1001:
admins:x:1002:alice
[dora@localhost ~]$ sudo cat /etc/passwd | grep alice
alice:x:1001:1001::/home/alice:/bin/bash


[dora@localhost ~]$ sudo groupadd bob
[dora@localhost ~]$ sudo useradd -m -g bob -G admins -s /bin/bash bob
[dora@localhost ~]$ sudo passwd bob
Changing password for user bob.
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.
[dora@localhost ~]$ sudo cat /etc/group | grep bob
admins:x:1002:alice,bob,charlie
bob:x:1003:
[dora@localhost ~]$ sudo cat /etc/passwd | grep bob
bob:x:1002:1003::/home/bob:/bin/bash


[dora@localhost ~]$ sudo groupadd charlie
[dora@localhost ~]$ sudo useradd -m -g charlie -G admins -s /bin/bash charlie
[dora@localhost ~]$ sudo passwd charlie
Changing password for user charlie.
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.
[dora@localhost ~]$ sudo cat /etc/group | grep charlie
admins:x:1002:alice,bob,charlie
charlie:x:1004:
[dora@localhost ~]$ sudo cat /etc/passwd | grep charlie
charlie:x:1003:1004::/home/charlie:/bin/bash


[dora@localhost ~]$ sudo groupadd eve
[dora@localhost ~]$ sudo useradd -m -g eve -s /bin/bash eve
[dora@localhost ~]$ sudo passwd eve
Changing password for user eve.
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.
[dora@localhost ~]$ sudo cat /etc/group | grep eve
eve:x:1005:
[dora@localhost ~]$ sudo cat /etc/passwd | grep eve
eve:x:1004:1005::/home/eve:/bin/bash


[dora@localhost ~]$ sudo groupadd backup
[dora@localhost ~]$ sudo useradd -m -g backup -s /usr/bin/nologin -d /var/backup backup
useradd: Warning: missing or non-executable shell '/usr/bin/nologin'
[dora@localhost ~]$ sudo passwd backup
Changing password for user backup.
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.
[dora@localhost ~]$ sudo cat /etc/group | grep backup
backup:x:1006:
[dora@localhost ~]$ sudo cat /etc/passwd | grep backup
backup:x:1005:1006::/var/backup:/usr/bin/nologin
```

🌞 **La conf `sudo` doit être la suivante**

| Qui est concerné | Quels droits                                                      | Doit fournir son password |
| ---------------- | ----------------------------------------------------------------- | ------------------------- |
| Groupe admins    | Tous les droits                                                   | Non                       |
| User eve         | Peut utiliser la commande `ls` en tant que l'utilisateur `backup` | Oui                       |

```
[dora@localhost ~]$ sudo cat /etc/sudoers | grep admin
%admins ALL=(ALL)	NOPASSWD: ALL
```

```
[dora@localhost ~]$ sudo cat /etc/sudoers | grep eve
eve	ALL=(backup)	/bin/ls
```

🌞 **Le dossier `/var/backup`**

- créez-le
- choisir des permissions les plus restrictives possibles (comme toujours, la base quoi) sachant que :
  - l'utilisateur `backup` doit pouvoir évoluer normalement dedans
  - les autres n'ont aucun droit
- il contient un fichier `/var/backup/precious_backup`
  - créez-le (contenu vide ou balec)
  - choisir des permissions les plus restrictives possibles sachant que
    - `backup` doit être le seul à pouvoir le lire et le modifier
    - le groupe `backup` peut uniquement le lire

```
[dora@localhost var]$ ls | grep backup
backup
```

```
[dora@localhost var]$ sudo chown backup:backup /var/backup/
[dora@localhost var]$ sudo chmod 700 /var/backup
```

```
[dora@localhost var]$ sudo cat /var/backup/precious_backup




```

🌞 **Mots de passe des users, prouvez que**

- ils sont hashés en SHA512 (c'est violent)
- ils sont salés (c'est pas une blague si vous connaissez pas le terme, on dit "salted" en anglais aussi)

```
[dora@localhost var]$ sudo chown backup:backup /var/backup/precious_backup
[dora@localhost var]$ sudo chmod 600 /var/backup/precious_backup
[dora@localhost var]$ sudo cat /etc/shadow | tail -n 5
alice:$6$JfwvAorDxp9ib93R$DJ9h5NKhafIJwnLjplN7hEJ8VPBcM06pS2Vb1zgBAly2ysR28s1VR0UTz4/v8MdaCa/LHgQwuD596Q/0dw..U.:19748:0:99999:7:::
bob:$6$0LbC0GwkYZ3kBzuC$uDHlt0fygRum/ciNToMLpmvywS8HkTvkqgJduoCBAoNw/fJq6gPQXtsy/HH5Thkj2w83sOJp1A3RCVCu9RR4x/:19748:0:99999:7:::
charlie:$6$M1rZTYGnBct14DMv$1KZ49vjbjOYDfEknp/cuSenUAsfWn.LrEVeHIUdFvmvyB/eQqEq0VmLnHfq0GzXU4DHVlV0uLrqBczrwpR4d0.:19748:0:99999:7:::
eve:$6$SLXDlhvJrBTbFaPf$Mtui.ChIUabbuvAdtEduEDay6.LSjm31riQThywWHI0cHNixyTI2vbj6AMqs32rJ2DxGCi3d9AUGAZd9kdgdj0:19748:0:99999:7:::
backup:$6$bGKDsnO0xxDBghq/$AyD9kry0cE48ObP4VvJ5gG5jN280ywrrm9VJDNFq1KAdpodqQZah2DS3Fg.qYGSuROZ7iyFCaZvigzcomRYNE0:19748:0:99999:7:::
```

🌞 **User eve**

- elle ne peut que saisir `sudo ls` et rien d'autres avec `sudo`
- vous pouvez faire `sudo -l` pour voir vos droits `sudo` actuels

```
[dora@localhost var]$ sudo -l -U eve
Matching Defaults entries for eve on localhost:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin,
    env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS",
    env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE",
    env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES",
    env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE",
    env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY",
    secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User eve may run the following commands on localhost:
    (backup) /bin/ls
```

# III. Gestion du temps

🌞 **Je vous laisse gérer le bail vous-mêmes**

- déterminez quel service sur Rocky Linux est le client NTP par défaut
  - demandez à google, ou explorez la liste des services avec `systemctl list-units -t service -a`, ou les deux
- demandez à ce service de se synchroniser sur [les serveurs français du NTP Pool Project](https://www.ntppool.org/en/zone/fr)
- assurez-vous que vous êtes synchronisés sur l'heure de Paris

```
[dora@localhost var]$ systemctl list-units -t service -a
  UNIT                                                                         >
  auditd.service                                                               >
● autofs.service                                                               >
  chronyd.service                                                              >
  crond.service                                                                >
  dbus-broker.service                                                          >
● display-manager.service                                                      >
  dm-event.service                                                             >
  dnf-makecache.service                                                        >
  dracut-cmdline.service                                                       >
  dracut-initqueue.service                                                     >
  dracut-mount.service                                                         >
  dracut-pre-mount.service                                                     >
  dracut-pre-pivot.service                                                     >
  dracut-pre-trigger.service                                                   >
  dracut-pre-udev.service                                                      >
  dracut-shutdown-onfailure.service                                            >
  dracut-shutdown.service                                                      >
● ebtables.service                                                             >
  emergency.service                                                            >
  firewalld.service                                                            >
  getty@tty1.service                                                           >
  initrd-cleanup.service  
[dora@localhost var]$ systemctl list-units -t service -a | grep ntp
  initrd-parse-etc.service                                                                  loaded    inactive dead    Mountpoints Configured in the Real Root
● ntpd.service                                                                              not-found inactive dead    ntpd.service
● ntpdate.service                                                                           not-found inactive dead    ntpdate.service
● sntp.service                                                                              not-found inactive dead    sntp.service

[dora@localhost var]$ cat /etc/chrony.conf | grep fr.pool
server fr.pool.ntp.org iburst

[dora@localhost var]$ sudo systemctl restart chronyd


[dora@localhost var]$ timedatectl
               Local time: Fri 2024-01-26 11:43:01 CET
           Universal time: Fri 2024-01-26 10:43:01 UTC
                 RTC time: Fri 2024-01-26 10:43:01
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```
