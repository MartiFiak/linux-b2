# TP2 Commun : Stack PHP

## Sommaire

- [TP2 Commun : Stack PHP](#tp2-commun--stack-php)
  - [Sommaire](#sommaire)
- [0. Setup](#0-setup)
- [I. Packaging de l'app PHP](#i-packaging-de-lapp-php)

# I. Packaging de l'app PHP

🌞 **`docker-compose.yml`**

- genre `tp2/php/docker-compose.yml` dans votre dépôt git de rendu
- votre code doit être à côté dans un dossier `src` : `tp2/php/src/tous_tes_bails.php`
- s'il y a un script SQL qui est injecté dans la base à son démarrage, il doit être dans `tp2/php/sql/seed.sql`
  - on appelle ça "seed" une database quand on injecte le schéma de base et éventuellement des données de test
- bah juste voilà ça doit fonctionner : je git clone ton truc, je `docker compose up` et ça doit fonctionne :)
- ce serait cool que l'app affiche un truc genre `App is ready on http://localhost:80` truc du genre dans les logs !

➜ **Un environnement de dév local propre avec Docker**

- 3 conteneurs, donc environnement éphémère/destructible
- juste un **`docker-compose.yml`** donc facilement transportable
- TRES facile de mettre à jour chacun des composants si besoin
  - oh tiens il faut ajouter une lib !
  - oh tiens il faut une autre version de PHP !
  - tout ça c'est np

```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  cat docker-compose.yml
version: "3"

services:
  php_apache:
    build: .
    ports:
      - "80:80"
    volumes:
      - ./src:/var/www/html

  mysql:
    image: mysql
    environment:
      MYSQL_ROOT_PASSWORD: root_password
      MYSQL_DATABASE: database_name
      MYSQL_USER: user_name
      MYSQL_PASSWORD: user_password
    volumes:
      - ./sql:/docker-entrypoint-initdb.d

  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    environment:
      PMA_HOST: mysql
    ports:
      - "8080:80"



 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php/sql  cat seed.sql 
-- seed.sql

CREATE TABLE IF NOT EXISTS example_table (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

INSERT INTO example_table (name) VALUES ('Example Data 1'), ('Example Data 2');

```

[docker-compose.yml](tp2/php/docker-compose.yml)

# TP2 admins : PHP stack


## Sommaire

- [TP2 admins : PHP stack](#tp2-admins--php-stack)
  - [Sommaire](#sommaire)
- [I. Good practices](#i-good-practices)
- [II. Reverse proxy buddy](#ii-reverse-proxy-buddy)
  - [A. Simple HTTP setup](#a-simple-http-setup)
  - [B. HTTPS auto-signé](#b-https-auto-signé)
  - [C. HTTPS avec une CA maison](#c-https-avec-une-ca-maison)

# I. Good practices

🌞 **Limiter l'accès aux ressources**

- limiter la RAM que peut utiliser chaque conteneur à 1G
- limiter à 1CPU chaque conteneur

```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  cat docker-compose.yml
version: '3'

services:
  php_apache:
    build: .
    image: php:apache
    ports:
      - "80:80"
    volumes:
      - ./src:/var/www/html
    deploy:
      resources:
        limits:
          cpus: "1"
          memory: 1G
    user: "1000:1000"

  mysql:
    image: mysql
    environment:
      MYSQL_ROOT_PASSWORD: root_password
      MYSQL_DATABASE: database_name
      MYSQL_USER: user_name
      MYSQL_PASSWORD: user_password
    volumes:
      - ./sql:/docker-entrypoint-initdb.d
    deploy:
      resources:
        limits:
          cpus: "1"
          memory: 1G

  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    environment:
      PMA_HOST: mysql
    ports:
      - "8080:80"
    deploy:
      resources:
        limits:
          cpus: "1"
          memory: 1G
    user: "1000:1000"

  nginx:
    image: nginx
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - "443:443"
    depends_on:
      - php_apache
      - phpmyadmin
    deploy:
      resources:
        limits:
          cpus: "1"
          memory: 1G
    user: "1000:1000"



 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  cat nginx.conf
server {
    listen 80;

    server_name www.supersite.com;

    location / {
        proxy_pass http://php_apache;
    }
}

server {
    listen 80;

    server_name pma.supersite.com;

    location / {
        proxy_pass http://phpmyadmin;
    }
}


 martyfiakotron@MartyFiakOTron-dellg155510  ~  cat /etc/hosts
127.0.0.1  localhost
127.0.0.1  www.supersite.com
127.0.0.1  pma.supersite.com
```


## B. HTTPS auto-signé

🌞 **HTTPS** auto-signé

- générez un certificat et une clé auto-signés
- adaptez la conf de NGINX pour tout servir en HTTPS
- la clé et le certificat doivent être montés avec des volumes (`-v`)
- la commande pour générer une clé et un cert auto-signés :

```bash
openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout www.supersite.com.key -out www.supersite.com.crt
```

```
 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout www.supersite.com.key -out www.supersite.com.crt
.......+......+..........+...+.........+..+...............+......+...+.........+...+...+.......+...+.....+....+............+........+...+.......+.....+.+..+++++++++++++++++++++++++++++++++++++++++++++*.+..+..........+..............+...+...+..........+.........+.........+...........+....+..+..........+.....+....+...+..+....+.........+..+.+++++++++++++++++++++++++++++++++++++++++++++*.....+.......+...+..+.....................+.............+.....+.......+.....+....+.....+...+...+........................+...............+...+.+......+.........+.....+.+...+..................+.........+........+..........+...........+..........+.........+..+....+..............+.+.........+.......................+....+........+.......+.........+...........+...............+..........+........+.............+...+......+.....+...+......+.+......+.....+.......+..................+............+..+...+...............+................+...............+..+...+...............+...................+...+..................+......+...........+....+...+..+....+...........+.............+..+.+.....+...............+.........+.+..+...............+...+.............+......+.....................+........+....+............+.....+......+..........+..+.+...............+.....+..........+...........+.+..+.......+.....+......+.......+..+.........+....+......+.....+......+.............+.....+.+..+............+...+...+.+......+.........+.....+................+.........+.........+.................+....+..............+.+...........+..................+............+...+....+.......................+.......+......+......+.......................+..........+............+........+.......+...+.................+....+..+...+......+....+......+...+...............+.....+..........+...........+.+......+.........+...+...+..+...+.........+................+...........................+...+...+..............+......+......+.+........+.+...+..+...................+.....+..........+..+..................+....+...+.................+.............+..+...+...+.+......+............+...+.....+...+......+....+.................+..........+..+.............+..+......+.......+...+.........+..+........................+....+...+...+.........+......+..+....+...............+.....+..........+.....+...+..........+.....+............+.+........+....+...............+........+...+.+...+...+..............+....+...........+...+.+.....+.........+...+........................+...+.+......+...+.....+.+......+....................+..................+.+.....+.......+............+...............+......+....................+..........+..+............+.+..............+...+...+............+......................+............+...+.....+...+.......+...+..+......+....+......+.....+.+..............................+...+...+........+......+...+...+.......+........+...+.....................+.+...+.....+.......+.....+...+.......+.........+...+...+............+..+......+.........+...+................+......+......+..+.+............+............+...........+.+.........+........................+..+..........+...+......+.....+..........+...............+..+..........+..+............+.............+..+.+...+......+........+...+..................+......+..........+.....+.+............+..+...+......+....+...........+.........+.....................+.+........+...............+......+......+.........+.......+.....+....+...+......+.....+.........+...+....+.....+.............+............+.........+...........+.+...+..+....+...+...+........+.+.....+....+..+.........+.+.........+.........+............+...+..+....+......+.......................+.+...+..+......+...+++++
.....+...+.+............+...+......+.....+.+..+......+....+...+...+..+.............+..+.......+......+..+...+++++++++++++++++++++++++++++++++++++++++++++*.+++++++++++++++++++++++++++++++++++++++++++++*..+......+......+.......+..+.............+...............+.....+.+.........+...............+.....+..........+..+...+......+.+......+...+.....+.+.....................+..............+...............+...+.+.....+....+............+...+...+..............+...+.......+.....+.............+...........+....+.....+.......+.....+...+.............+.........+...+...+..+...............+......+............+....+........................+..............+.+..+..........+........+.+...+.....+.+...........+.+...............+..+.............+.....+.........+..........+.....+................+..+......+....+..................+..+.........................+........+...+..............................+.+........+.+..+.........+...+............+...+......+....+..+..........+.........+..............+...+.......+.....+...+...................+.....+.+...+.................+..........+........+.+...+..+......+..........+.....+....+.........+.....+.+...+.....+..........+.................+...+...+....+...+...........+....+........+...+....+............+.........+..+..................+....+..+...+.+...........+..........+........+......+.+...+......+.....+......+...+.+.........+.....+......+....+...+..+.+..+.........+.+..................+........................+...............+.....................+..+.......+...+.....+...................+.....+.+...+...........+.........+.+...+...........+......+.+..+.+........................+...+...+...........+.+...+.....+.......+........+.+.....+.+.........+..............+...................+..+......+....+......+.....+.......+...+.........+...........+....+..................+...........+....+...............+...........+.......+..+.+..........................+......+...+....+........+....+......+.................+.......+........+...+..........+.......................+......+...+...............+............+.+...+..+.......+..+....+......+...+..+....+.....+....+.....+...+..........+.........+......+...+............+.....+.......+...+...+.........+...+............+..+...+......+..........+..+....+.......................+.+.........+........+...+......+.........+....+....................+++++
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:
Email Address []:


 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  cat nginx.conf 
server {
    listen 80;

    server_name www.supersite.com;

    location / {
        proxy_pass http://php-apache;
    }
}

server {
    listen 443 ssl;

    server_name www.supersite.com;

    ssl_certificate /etc/nginx/ssl/www.supersite.com.crt;
    ssl_certificate_key /etc/nginx/ssl/www.supersite.com.key;

    location / {
        proxy_pass http://php_apache;
    }
}
```

## C. HTTPS avec une CA maison

🌞 **Générer une clé et un certificat de CA**

```bash
# mettez des infos dans le prompt, peu importe si c'est fake
# on va vous demander un mot de passe pour chiffrer la clé aussi
$ openssl genrsa -des3 -out CA.key 4096
$ openssl req -x509 -new -nodes -key CA.key -sha256 -days 1024  -out CA.pem
$ ls
# le pem c'est le certificat (clé publique)
# le key c'est la clé privée
```

```
 ✘ martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  openssl genrsa -des3 -out CA.key 4096
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:


 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  openssl req -x509 -new -nodes -key CA.key -sha256 -days 1024  -out CA.pem
Enter pass phrase for CA.key:
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:
Email Address []:


 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  ls
CA.key  docker-compose.yml  nginx       sql  www.supersite.com.crt
CA.pem  Dockerfile          nginx.conf  src  www.supersite.com.key
```


🌞 **Générer une clé et une demande de signature de certificat pour notre serveur web**

```bash
$ openssl req -new -nodes -out www.supersite.com.csr -newkey rsa:4096 -keyout www.supersite.com.key
$ ls
# www.supersite.com.csr c'est la demande de signature
# www.supersite.com.key c'est la clé qu'utilisera le serveur web
```

```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  openssl req -new -nodes -out www.supersite.com.csr -newkey rsa:4096 -keyout www.supersite.com.key
.........+.....+.+........+....+..+.+...+..+.......+..+...+...+.......+.....+..........+............+.....+....+..+.......+..+.........+.+.....+....+..+.......+...............+++++++++++++++++++++++++++++++++++++++++++++*.....+...+.+..............+.........+.+..+...+.+++++++++++++++++++++++++++++++++++++++++++++*.....+....+......+...............+..............+.+++++
..+.....+....+......+..............+...+...+.+...+......+......+++++++++++++++++++++++++++++++++++++++++++++*.+++++++++++++++++++++++++++++++++++++++++++++*.+..+.+......+.....+++++
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:
Email Address []:

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:


 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  ls
CA.key              Dockerfile  sql                    www.supersite.com.csr
CA.pem              nginx       src                    www.supersite.com.key
docker-compose.yml  nginx.conf  www.supersite.com.crt
```

🌞 **Faire signer notre certificat par la clé de la CA**

- préparez un fichier `v3.ext` qui contient :

```ext
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = www.supersite.com
DNS.2 = www.tp7.secu
```

- effectuer la demande de signature pour récup un certificat signé par votre CA :

```bash
$ openssl x509 -req -in www.supersite.com.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out www.supersite.com.crt -days 500 -sha256 -extfile v3.ext
$ ls
# www.supersite.com.crt c'est le certificat qu'utilisera le serveur web
```

```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  cat v3.ext 
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = www.supersite.com
DNS.2 = www.tp7.secu


 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  openssl x509 -req -in www.supersite.com.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out www.supersite.com.crt -days 500 -sha256 -extfile v3.ext
Certificate request self-signature ok
subject=C=AU, ST=Some-State, O=Internet Widgits Pty Ltd
Enter pass phrase for CA.key:


 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php  ls
CA.key              Dockerfile  src                    www.supersite.com.key
CA.pem              nginx       v3.ext
CA.srl              nginx.conf  www.supersite.com.crt
docker-compose.yml  sql         www.supersite.com.csr
```


🌞 **Ajustez la configuration NGINX**

- le site web doit être disponible en HTTPS en utilisant votre clé et votre certificat
- une conf minimale ressemble à ça :

```nginx
server {
    [...]
    # faut changer le listen
    listen 10.7.1.103:443 ssl;

    # et ajouter ces deux lignes
    ssl_certificate /chemin/vers/le/cert/www.supersite.com.crt;
    ssl_certificate_key /chemin/vers/la/clé/www.supersite.com.key;
    [...]
}
```

🌞 **Prouvez avec un `curl` que vous accédez au site web**

- depuis votre PC
- avec un `curl -k` car il ne reconnaît pas le certificat là

```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/tp2/php curl -k https://www.supersite.com
<!DOCTYPE html>
<html>
<head>
    <title>Super Site</title>
</head>
<body>
    <h1>Bienvenue sur Super Site!</h1>
    <p>Ce site est en construction.</p>
</body>
</html>
```

🌞 **Ajouter le certificat de la CA dans votre navigateur**

- vous pourrez ensuite visitez `https://web.tp7.b2` sans alerte de sécurité, et avec un cadenas vert
- il est nécessaire de joindre le site avec son nom pour que HTTPS fonctionne (fichier `hosts`)
