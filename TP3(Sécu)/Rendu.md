# TP3 : Linux Hardening

## Sommaire

- [TP3 : Linux Hardening](#tp3--linux-hardening)
  - [Sommaire](#sommaire)
  - [0. Setup](#0-setup)
  - [1. Guides CIS](#1-guides-cis)
  - [2. Conf SSH](#2-conf-ssh)
  - [4. DoT](#4-dot)
  - [5. AIDE](#5-aide)

## 1. Guides CIS

🌞 **Suivre un guide CIS**

- téléchargez le guide CIS de Rocky 9 [ici](https://downloads.cisecurity.org/#/)
- vous devez faire :
  - la section 2.1
  - les sections 3.1 3.2 et 3.3
  - toute la section 5.2 Configure SSH Server
  - au moins 10 points dans la section 6.1 System File Permissions
  - au moins 10 points ailleur sur un truc que vous trouvez utile

```
[dora@localhost ~]$ rpm -q chrony
chrony-4.1-3.el9.rocky.0.1.x86_64

a@localhost ~]$ grep -E "^(server|pool)" /etc/chrony.conf
pool 2.rhel.pool.ntp.org iburst

[dora@localhost ~]$  grep ^OPTIONS /etc/sysconfig/chronyd
OPTIONS="-u chrony"
OPTIONS="-F 2"
```

```
[dora@localhost ~]$ grep -Pqs '^\h*0\b' /sys/module/ipv6/parameters/disable && echo -e "\n -
IPv6 is enabled\n" || echo -e "\n - IPv6 is not enabled\n"

 -
IPv6 is enabled

[dora@localhost ~]$ bash script3.1.2.sh

- Audit Result:
 ** PASS **

 - System has no wireless NICs installed

 [dora@localhost ~]$ cat script3.1.3.sh
#!/usr/bin/env bash
{
 l_output="" l_output2=""
 l_mname="tipc" # set module name
 # Check if the module exists on the system
 if [ -z "$(modprobe -n -v "$l_mname" 2>&1 | grep -Pi --
"\h*modprobe:\h+FATAL:\h+Module\h+$l_mname\h+not\h+found\h+in\h+directory")"
]; then
 # Check how module will be loaded
 l_loadable="$(modprobe -n -v "$l_mname")"
 [ "$(wc -l <<< "$l_loadable")" -gt "1" ] && l_loadable="$(grep -P --
"(^\h*install|\b$l_mname)\b" <<< "$l_loadable")"
 if grep -Pq -- '^\h*install \/bin\/(true|false)' <<< "$l_loadable";
then
 l_output="$l_output\n - module: \"$l_mname\" is not loadable:
\"$l_loadable\""
 else
 l_output2="$l_output2\n - module: \"$l_mname\" is loadable:
\"$l_loadable\""
 fi
 # Check is the module currently loaded
 if ! lsmod | grep "$l_mname" > /dev/null 2>&1; then
 l_output="$l_output\n - module: \"$l_mname\" is not loaded"
 else
 l_output2="$l_output2\n - module: \"$l_mname\" is loaded"
 fi
 # Check if the module is deny listed
 if modprobe --showconfig | grep -Pq -- "^\h*blacklist\h+$l_mname\b";
then
 l_output="$l_output\n - module: \"$l_mname\" is deny listed in:
\"$(grep -Pl -- "^\h*blacklist\h+$l_mname\b" /etc/modprobe.d/*)\""
 else
 l_output2="$l_output2\n - module: \"$l_mname\" is not deny listed"
 fi
 else
 l_output="$l_output\n - Module \"$l_mname\" doesn't exist on the
system"
 fi
 # Report results. If no failures output in l_output2, we pass
 if [ -z "$l_output2" ]; then
 echo -e "\n- Audit Result:\n ** PASS **\n$l_output\n"
 else
 echo -e "\n- Audit Result:\n ** FAIL **\n - Reason(s) for audit
failure:\n$l_output2\n"
 [ -n "$l_output" ] && echo -e "\n- Correctly set:\n$l_output\n"
 fi
}

[dora@localhost ~]$ bash script3.1.3.sh

- Audit Result:
 ** PASS **

 - Module "tipc" doesn't exist on the
system
```

```
[dora@localhost ~]$ printf "
net.ipv4.ip_forward = 0
"

net.ipv4.ip_forward = 0

[dora@localhost ~]$ sudo sysctl -w net.ipv4.ip_forward=0
net.ipv4.ip_forward = 0

[dora@localhost ~]$ sudo sysctl -w net.ipv4.route.flush=1
net.ipv4.route.flush = 1

[dora@localhost ~]$ printf "
net.ipv6.conf.all.forwarding = 0
"

net.ipv6.conf.all.forwarding = 0

[dora@localhost ~]$ sudo sysctl -w net.ipv6.conf.all.forwarding=0
net.ipv6.conf.all.forwarding = 0

[dora@localhost ~]$ sudo sysctl -w net.ipv6.route.flush=1
net.ipv6.route.flush = 1

[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.all.send_redirects=0
net.ipv4.conf.all.send_redirects = 0
[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.default.send_redirects=0
net.ipv4.conf.default.send_redirects = 0
[dora@localhost ~]$ sudo sysctl -w net.ipv4.route.flush=1
net.ipv4.route.flush = 1
```

```
[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.all.accept_source_route=0
net.ipv4.conf.all.accept_source_route = 0
[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.default.accept_source_route=0
net.ipv4.conf.default.accept_source_route = 0

[dora@localhost ~]$ sudo sysctl -w net.ipv6.conf.all.accept_source_route=0
net.ipv6.conf.all.accept_source_route = 0
[dora@localhost ~]$ sudo sysctl -w net.ipv6.conf.default.accept_source_route=0
net.ipv6.conf.default.accept_source_route = 0

[dora@localhost ~]$ sudo sysctl -w net.ipv6.conf.all.accept_redirects=0
[sudo] password for dora:
net.ipv6.conf.all.accept_redirects = 0
[dora@localhost ~]$ sudo sysctl -w net.ipv6.conf.default.accept_redirects=0
net.ipv6.conf.default.accept_redirects = 0

[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.all.secure_redirects=0
net.ipv4.conf.all.secure_redirects = 0
[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.default.secure_redirects=0
net.ipv4.conf.default.secure_redirects = 0

[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.all.log_martians=1
net.ipv4.conf.all.log_martians = 1
[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.default.log_martians=1
net.ipv4.conf.default.log_martians = 1

[dora@localhost ~]$ sudo sysctl -w net.ipv4.icmp_echo_ignore_broadcasts=1
net.ipv4.icmp_echo_ignore_broadcasts = 1

[dora@localhost ~]$ sudo sysctl -w net.ipv4.icmp_ignore_bogus_error_responses=1
net.ipv4.icmp_ignore_bogus_error_responses = 1

[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.all.rp_filter=1
net.ipv4.conf.all.rp_filter = 1
[dora@localhost ~]$ sudo sysctl -w net.ipv4.conf.default.rp_filter=1
net.ipv4.conf.default.rp_filter = 1

[dora@localhost ~]$ sudo sysctl -w net.ipv4.tcp_syncookies=1
net.ipv4.tcp_syncookies = 1

[dora@localhost ~]$ sudo sysctl -w net.ipv6.conf.all.accept_ra=0
net.ipv6.conf.all.accept_ra = 0
[dora@localhost ~]$ sudo sysctl -w net.ipv6.conf.default.accept_ra=0
net.ipv6.conf.default.accept_ra = 0
```

```
[dora@localhost ~]$ stat -Lc "%n %a %u/%U %g/%G" /etc/ssh/sshd_config
/etc/ssh/sshd_config 600 0/root 0/root

[dora@localhost ~]$ sudo chown root:root /etc/ssh/sshd_config
[dora@localhost ~]$ sudo chmod u-x,go-rwx /etc/ssh/sshd_config

[dora@localhost ~]$ sudo systemctl reload sshd

[dora@localhost ~]$ sudo bash script5.2.2

- Audit Result:
 *** FAIL ***

 - File: "/etc/ssh/ssh_host_ecdsa_key" is mode "0640"
should be mode: "600" or more restrictive
 - File: "/etc/ssh/ssh_host_ecdsa_key" is owned by:
"" should be owned by "root"
 - File: "/etc/ssh/ssh_host_ecdsa_key" is owned by group
"" should belong to group "ssh_keys"
 - File: "/etc/ssh/ssh_host_ed25519_key" is mode "0640"
should be mode: "600" or more restrictive
 - File: "/etc/ssh/ssh_host_ed25519_key" is owned by:
"" should be owned by "root"
 - File: "/etc/ssh/ssh_host_ed25519_key" is owned by group
"" should belong to group "ssh_keys"
 - File: "/etc/ssh/ssh_host_rsa_key" is mode "0640"
should be mode: "600" or more restrictive
 - File: "/etc/ssh/ssh_host_rsa_key" is owned by:
"" should be owned by "root"
 - File: "/etc/ssh/ssh_host_rsa_key" is owned by group
"" should belong to group "ssh_keys"

 - Correctly
set:

[dora@localhost ~]$ sudo bash script5.2.3_2
 - Checking private key file: "/etc/ssh/ssh_host_ecdsa_key.pub"
 - File: "/etc/ssh/ssh_host_ecdsa_key.pub" is owned by: "" changing
owner to "root"
 - File: "/etc/ssh/ssh_host_ecdsa_key.pub" is owned by group ""
changing to group "root"
 - Checking private key file: "/etc/ssh/ssh_host_ed25519_key.pub"
 - File: "/etc/ssh/ssh_host_ed25519_key.pub" is owned by: "" changing
owner to "root"
 - File: "/etc/ssh/ssh_host_ed25519_key.pub" is owned by group ""
changing to group "root"
 - Checking private key file: "/etc/ssh/ssh_host_rsa_key.pub"
 - File: "/etc/ssh/ssh_host_rsa_key.pub" is owned by: "" changing
owner to "root"
 - File: "/etc/ssh/ssh_host_rsa_key.pub" is owned by group ""
changing to group "root"

[dora@localhost ~]$ echo "AllowUsers user1 user2" | sudo tee -a /etc/ssh/sshd_config
AllowUsers user1 user2
[dora@localhost ~]$ echo "AllowGroups sshusers admins" | sudo tee -a /etc/ssh/sshd_config
AllowGroups sshusers admins
[dora@localhost ~]$ echo "DenyUsers baduser1 baduser2" | sudo tee -a /etc/ssh/sshd_config
DenyUsers baduser1 baduser2
[dora@localhost ~]$ echo "DenyGroups badgroup1 badgroup2" | sudo tee -a /etc/ssh/sshd_config
DenyGroups badgroup1 badgroup2
[dora@localhost ~]$ sudo systemctl restart sshd

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -Pi '^\h*(allow|deny)(users|groups)\h+\H+(\h+.*)?$'
allowusers user1
allowusers user2
denyusers baduser1
denyusers baduser2
allowgroups sshusers
allowgroups admins
denygroups badgroup1
denygroups badgroup2

[dora@localhost ~]$ sudo grep -Pi '^\h*(allow|deny)(users|groups)\h+\H+(\h+.*)?$' /etc/ssh/sshd_config /etc/ssh/sshd_config.d/*.conf
/etc/ssh/sshd_config:AllowUsers user1 user2
/etc/ssh/sshd_config:AllowGroups sshusers admins
/etc/ssh/sshd_config:DenyUsers baduser1 baduser2
/etc/ssh/sshd_config:DenyGroups badgroup1 badgroup2

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep loglevel
loglevel INFO

[dora@localhost ~]$ sudo grep -Pi -- '^\h*loglevel' /etc/ssh/sshd_config /etc/ssh/sshd_config.d/*.co
nf | grep -Evi '(VERBOSE|INFO)'

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc
/hosts | awk '{print $1}')" | grep -i usepam
usepam yes

[dora@localhost ~]$ sudo grep -Pi '^\h*UsePAM\b' /etc/ssh/sshd_config /etc/ssh/sshd_config.d/*.conf | grep -Evi 'yes'
grep: /etc/ssh/sshd_config.d/*.conf: No such file or directory

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc
/hosts | awk '{print $1}')" | grep permitrootlogin
permitrootlogin without-password

[dora@localhost ~]$ sudo grep -Pi '^\h*PermitRootLogin\b' /etc/ssh/sshd_config /etc/ssh/sshd_config.d/*.conf | grep -Evi 'no'

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep permitrootlogin
permitrootlogin no

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc
/hosts | awk '{print $1}')" | grep hostbasedauthentication
hostbasedauthentication no

[dora@localhost ~]$ sudo grep -Pi '^\h*HostbasedAuthentication\b' /etc/ssh/sshd_config /etc/ssh/sshd_config.d/*.conf | grep -Evi 'no'

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc
/hosts | awk '{print $1}')" | grep permitemptypasswords
permitemptypasswords no

[dora@localhost ~]$ sudo grep -Pi '^\h*PermitEmptyPasswords\b' /etc/ssh/sshd_config /etc/ssh/sshd_config.d/*.conf | grep -Evi 'no'

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc
/hosts | awk '{print $1}')" | grep permituserenvironment
permituserenvironment no

[dora@localhost ~]$ sudo  grep -Pi '^\h*PermitUserEnvironment\b' /etc/ssh/sshd_config /etc/ssh/sshd_
config.d/*.conf | grep -Evi 'no'

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc
/hosts | awk '{print $1}')" | grep ignorerhosts
ignorerhosts yes

[dora@localhost ~]$ sudo grep -Pi '^\h*ignorerhosts\b' /etc/ssh/sshd_config /etc/ssh/sshd_config.d/*
.conf | grep -Evi 'yes'

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc
/hosts | awk '{print $1}')" | grep -i x11forwarding
x11forwarding no

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -i allowtcpforwarding
allowtcpforwarding no

[dora@localhost ~]$ sudo grep -i '^\s*CRYPTO_POLICY=' /etc/sysconfig/sshd /etc/ssh/sshd_config.d/*.c
onf

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep banner
banner /etc/issue.net

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep maxauthtries
maxauthtries 4

[dora@localhost ~]$ sudo grep -Pi '^\h*maxauthtries\h+([5-9]|[1-9][0-9]+)' /etc/ssh/sshd_config /etc/ssh/sshd_config.d/*.conf | while read -r l_out; do sed -ri "/^\s*maxauthtries\s+([5-9]|[1-9][0-9]+)/s/^/# /" "$(awk -F: '{print $1}' <<< $l_out)";done

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -i maxstartups
maxstartups 10:30:60

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc
/hosts | awk '{print $1}')" | grep -i maxsessions
maxsessions 10

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep logingracetime
logingracetime 60

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep clientaliveinterval
clientaliveinterval 15

[dora@localhost ~]$ sudo sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc
/hosts | awk '{print $1}')" | grep clientalivecountmax
clientalivecountmax 3
```

```
[dora@localhost ~]$ stat -Lc "%n %a %u/%U %g/%G" /etc/passwd
/etc/passwd 644 0/root 0/root

[dora@localhost ~]$ stat -Lc "%n %a %u/%U %g/%G" /etc/passwd-
/etc/passwd- 644 0/root 0/root

[dora@localhost ~]$ stat -Lc "%n %a %u/%U %g/%G" /etc/group
/etc/group 644 0/root 0/root

[dora@localhost ~]$ stat -Lc "%n %a %u/%U %g/%G" /etc/group-
/etc/group- 644 0/root 0/root

[dora@localhost ~]$ stat -Lc "%n %a %u/%U %g/%G" /etc/shadow
/etc/shadow 0 0/root 0/root

[dora@localhost ~]$ stat -Lc "%n %a %u/%U %g/%G" /etc/shadow-
/etc/shadow- 0 0/root 0/root

[dora@localhost ~]$ stat -Lc "%n %a %u/%U %g/%G" /etc/gshadow
/etc/gshadow 0 0/root 0/root

[dora@localhost ~]$ stat -Lc "%n %a %u/%U %g/%G" /etc/gshadow-
/etc/gshadow- 0 0/root 0/root

[dora@localhost ~]$ sudo df --local -P | sudo awk '{if (NR!=1) print $6}' | sudo xargs -I '{}' find
'{}' -xdev -type f -perm -0002

[dora@localhost ~]$ sudo df --local -P | sudo awk {'if (NR!=1) print $6'} | sudo xargs -I '{}' find '{}' -xdev -nouser
```

```
[dora@localhost ~]$ sudo awk -F: '($2 != "x" ) { print $1 " is not set to shadowed passwords "}' /etc/passwd

[dora@localhost ~]$ sudo awk -F: '($2 == "" ) { print $1 " does not have a password "}' /etc/shadow

[dora@localhost ~]$ sudo bash script6.2.3

[dora@localhost ~]$ sudo bash script6.2.4

[dora@localhost ~]$ sudo bash script6.2.5

[dora@localhost ~]$ sudo bash script6.2.6

[dora@localhost ~]$ sudo bash script6.2.7

[dora@localhost ~]$ sudo bash script6.2.8

[dora@localhost ~]$ sudo awk -F: '($3 == 0) { print $1 }' /etc/passwd
root
```

## 2. Conf SSH

![SSH](./img/ssh.jpg)

🌞 **Chiffrement fort côté serveur**

- trouver une ressource de confiance (je veux le lien en compte-rendu)
- configurer le serveur SSH pour qu'il utilise des paramètres forts en terme de chiffrement (je veux le fichier de conf dans le compte-rendu)
  - conf dans le fichier de conf
  - regénérer des clés pour le serveur ?
  - regénérer les paramètres Diffie-Hellman ? (se renseigner sur Diffie-Hellman ?)

```
https://man.openbsd.org/sshd_config
```

```
  [dora@localhost ~]$ sudo cat /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.104 2021/07/02 05:11:21 dtucker Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# To modify the system-wide sshd configuration, create a  *.conf  file under
#  /etc/ssh/sshd_config.d/  which will be automatically included below
Include /etc/ssh/sshd_config.d/*.conf

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
#Port 22
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
PermitRootLogin no
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

PasswordAuthentication no
PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile      .ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no

# Change to no to disable s/key passwords
#KbdInteractiveAuthentication yes

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the KbdInteractiveAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via KbdInteractiveAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and KbdInteractiveAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in RHEL and may cause several
# problems.
#UsePAM no

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
#X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
#       ForceCommand cvs server
AllowUsers user1 user2
AllowGroups sshusers admins
DenyUsers baduser1 baduser2
DenyGroups badgroup1 badgroup2

Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
```

```
PS C:\Users\ZerBr> ssh-keygen -t ed25519 -f .ssh/id_ed25519
Generating public/private ed25519 key pair.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in .ssh/id_ed25519
Your public key has been saved in .ssh/id_ed25519.pub
The key fingerprint is:
SHA256:oLIZoPCG5dyNu1GL+wZqWUqz23hitCbYtZC365IlxhQ zerbr@MartyFiakoTron
The key's randomart image is:
+--[ED25519 256]--+
|                 |
|  E              |
|o ..  .          |
|oB.. + .         |
|ooB.+ o S        |
| .X=*+ .         |
|.=o/++.          |
|o #o++.          |
| =o**+.          |
+----[SHA256]-----+
```

🌞 **Clés de chiffrement fortes pour le client**

- trouver une ressource de confiance (je veux le lien en compte-rendu)
- générez-vous une paire de clés qui utilise un chiffrement fort et une passphrase
- ne soyez pas non plus absurdes dans le choix du chiffrement quand je dis "fort" (genre pas de RSA avec une clé de taile 98789080932083209 bytes)

```
https://man.openbsd.org/ssh-keygen
```

```
PS C:\Users\ZerBr> ssh-keygen -t ed25519 -f C:\Users\ZerBr\.ssh\id_ed25519 -C "your_email@example.com"
Generating public/private ed25519 key pair.
C:\Users\ZerBr\.ssh\id_ed25519 already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\ZerBr\.ssh\id_ed25519
Your public key has been saved in C:\Users\ZerBr\.ssh\id_ed25519.pub
The key fingerprint is:
SHA256:jpHyj3ZQsefM/ckfXEM+/dw+/GUDPSE66hgyq94DxDU your_email@example.com
The key's randomart image is:
+--[ED25519 256]--+
|                 |
|     E  .        |
|  . . .  o   . o |
|   o   .o . . = o|
|  . . o.S= + . *o|
|   . o.+  = o o.B|
|    .ooo..   o.=*|
|    ..=o=     +=+|
|  .o.+oo..     .*|
+----[SHA256]-----+
```



🌞 **Connectez-vous en SSH à votre VM avec cette paire de clés**

- prouvez en ajoutant `-vvvv` sur la commande `ssh` de connexion que vous utilisez bien cette clé là

```
PS C:\Users\ZerBr> ssh -vvvv -i ~/.ssh/id_ed25519 dora@10.7.2.100
debug1: Authentications that can continue: publickey
debug3: start over, passed a different list publickey
debug3: preferred gssapi-keyex,gssapi-with-mic,publickey,keyboard-interactive,password
debug3: authmethod_lookup publickey
debug3: remaining preferred: keyboard-interactive,password
debug3: authmethod_is_enabled publickey
debug1: Next authentication method: publickey
debug1: Offering public key: /path/to/your/.ssh/id_ed25519
debug3: send_pubkey_test
debug3: send packet: type 50
debug2: we sent a publickey packet, wait for reply
debug3: receive packet: type 51
debug1: Authentications that can continue: publickey
debug2: we did not send a packet, disable method
```

## 4. DoT

Ca commence à faire quelques années maintenant que plusieurs acteurs poussent pour qu'on fasse du DNS chiffré, et qu'on arrête d'envoyer des requêtes DNS en clair dans tous les sens.

Le Dot est une techno qui va dans ce sens : DoT pour DNS over TLS. On fait nos requêtes DNS dans des tunnels chiffrés avec le protocole TLS.

🌞 **Configurer la machine pour qu'elle fasse du DoT**

- installez `systemd-networkd` sur la machine pour ça
- activez aussi DNSSEC tant qu'on y est
- référez-vous à cette doc qui est cool par exemple
- utilisez le serveur public de CloudFlare : 1.1.1.1 (il supporte le DoT)

```
[dora@localhost ~]$ sudo dnf -y install systemd
Last metadata expiration check: 0:01:29 ago on Mon 15 Jan 2024 09:40:05 AM CET.
Package systemd-252-18.el9.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

```
[dora@localhost ~]$ sudo cat /etc/systemd/resolved.conf | tail -n 13 | head -n 5
DNS=1.1.1.1
#FallbackDNS=
#Domains=
DNSSEC=yes
DNSOverTLS=yes
```

🌞 **Prouvez que les requêtes DNS effectuées par la machine...**

- ont une réponse qui provient du serveur que vous avez conf (normalement c'est `127.0.0.1` avec `systemd-networkd` qui tourne)
  - quand on fait un `dig ynov.com` on voit en bas quel serveur a répondu
- mais qu'en réalité, la requête a été forward vers 1.1.1.1 avec du TLS
  - je veux une capture Wireshark à l'appui !

```
  [dora@localhost ~]$ dig ynov.com

; <<>> DiG 9.16.23-RH <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 40126
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               300     IN      A       104.26.11.233
ynov.com.               300     IN      A       104.26.10.233
ynov.com.               300     IN      A       172.67.74.226

;; Query time: 25 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Mon Jan 15 10:55:28 CET 2024
;; MSG SIZE  rcvd: 85
  ```

## 5. AIDE

Un truc demandé au point 1.3.1 du guide CIS c'est d'installer AIDE.

AIDE est un IDS ou *Intrusion Detection System*. Les IDS c'est un type de programme dont les fonctions peuvent être multiples.

Dans notre cas, AIDE, il surveille que certains fichiers du disque n'ont pas été modifiés. Des fichiers comme `/etc/shadow` par exemple.

🌞 **Installer et configurer AIDE**

- et bah incroyable mais [une très bonne ressource ici](https://www.it-connect.fr/aide-utilisation-et-configuration-dune-solution-de-controle-dintegrite-sous-linux/)
- configurez AIDE pour qu'il surveille (fichier de conf en compte-rendu)
  - le fichier de conf du serveur SSH
  - le fichier de conf du client chrony (le service qui gère le temps)
  - le fichier de conf de `systemd-networkd`

  ```
  [dora@localhost ~]$ sudo dnf install aide
Installed:
  aide-0.16-100.el9.x86_64

Complete!

[dora@localhost ~]$ sudo aide --init
Start timestamp: 2024-01-15 14:40:53 +0100 (AIDE 0.16)
AIDE initialized database at /var/lib/aide/aide.db.new.gz

Number of entries:      39055

---------------------------------------------------
The attributes of the (uncompressed) database(s):
---------------------------------------------------

/var/lib/aide/aide.db.new.gz
  MD5      : 8CrcjmTDBkhX2diBPDvLbg==
  SHA1     : NuUFF96Fl65GM3mKaJX2aZ0OiVw=
  RMD160   : zhazdVH1rWj+juzOV/pgOfqc8c0=
  TIGER    : a8kj071YEl7ByHoaMBa8HP7L+2UWN1b6
  SHA256   : eFEqwhH2Vu8KFdGLsO5dLlewvldPDvrf
             UJBGB806lSg=
  SHA512   : zzzoWQjJPVlHRyTcITstia5NwlaSNE3q
             94jOeZiL2Jkvd5mC2Y8BmsnYQb7jSFz6
             fSr9vJEr94pzo6lbcrWq4w==


End timestamp: 2024-01-15 14:41:17 +0100 (run time: 0m 24s)

[dora@localhost ~]$ sudo !!
sudo mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
```

[Capture wireshark]()

🌞 **Scénario de modification**

- introduisez une modification dans le fichier de conf du serveur SSH
- montrez que AIDE peut la détecter

```
[dora@localhost ~]$ sudo aide --check
AIDE found differences between database and filesystem!!
Start timestamp: 2024-01-17 15:32:08
Summary:
  Total number of files:        1000
  Added files:                  0
  Removed files:                0
  Changed files:                3

---------------------------------------------------
Added files:
---------------------------------------------------

---------------------------------------------------
Removed files:
---------------------------------------------------

---------------------------------------------------
Changed files:
---------------------------------------------------
/etc/chrony.conf
/etc/systemd/network/eth1.network
/etc/ssh/sshd_config
```

🌞 **Timer et service systemd**

- créez un service systemd qui exécute un check AIDE
  - il faut créer un fichier `.service` dans le dossier `/etc/systemd/system/`
  - contenu du fichier à montrer dans le compte rendu
- créez un timer systemd qui exécute un check AIDE toutes les 10 minutes
  - il faut créer un fichier `.timer` dans le dossier `/etc/systemd/system/`
  - il doit porter le même nom que le service, genre `aide.service` et `aide.timer`
  - c'est complètement irréaliste 10 minutes, mais ça vous permettra de faire des tests (vous pouvez même raccourcir encore)

```
[dora@localhost ~]$ sudo vim /etc/systemd/system/aide-check.service

[dora@localhost ~]$ sudo cat /etc/systemd/system/aide-check.service
[Unit]
Description=Run AIDE integrity check

[Service]
ExecStart=/usr/sbin/aide --check
Type=oneshot
```


```
[dora@localhost ~]$ sudo vim /etc/systemd/system/aide-check.service

[dora@localhost ~]$ sudo cat /etc/systemd/system/aide-check.timer
[Unit]
Description=Run AIDE integrity check every 10 minutes

[Timer]
OnCalendar=*:0/10
Persistent=true

[Install]
WantedBy=timers.target

[dora@localhost ~]$ sudo systemctl enable aide-check.timer
[dora@localhost ~]$ sudo systemctl start aide-check.timer

[dora@localhost ~]$ sudo systemctl status aide-check.timer
● aide-check.timer - Run AIDE integrity check every 10 minutes
   Loaded: loaded (/etc/systemd/system/aide-check.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Tue 2024-01-17 15:44:42 UTC; 10min ago
  Trigger: Tue 2024-01-17 15:44:42 UTC; 2min left
```
