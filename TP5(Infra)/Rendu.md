# Partie 1 : Setup du lab

## Sommaire

- [Partie 1 : Setup du lab](#partie-1--setup-du-lab)
  - [Sommaire](#sommaire)
  - [0. Setup](#0-setup)
  - [1. Lab initial](#1-lab-initial)
    - [A. Présentation](#a-présentation)
  - [B. L'app web](#b-lapp-web)
  - [C. Monter le lab](#c-monter-le-lab)


## 1. Lab initial

### A. Présentation

## B. L'app web

## C. Monter le lab

🌞 **A rendre**

- le `Vagrantfile`
- les **scripts** qui effectuent la conf
- le README explique juste qu'il faut `vagrant up` et éventuellement taper deux trois commandes après si nécessaire

- je veux des scripts `bash` qui font la conf à votre place
  - ce sera utile pour répliquer la conf sur d'autres machines
  - ça vous fait pratiquer le scripting
  - je vous ai re-uploadé le cours scripting de l'an dernier pour les synntaxes de base (là encore, faites appel à moi pour utiliser `bash`, la syntaxe fait (très) mal)
- avec Vagrant, vous pouvez faire un dossier partagé entre votre PC et la VM : idéal pour préparer des fichiers de conf ou des scripts et les déposer dans la VM
  - on peut même directement demander à Vagrant d'exécuter un script au démarrage de la VM
- je sais que vous en avez pas beaucoup fait des scripts, faites appel à moi avec plein de questions pour rendre le truc utile et efficace si besoin, c'est l'occasion de pratiquer justement

[Vagrantfile](Files/Vagrantfile)
[Scripts](Files/)


```
⚙ martyfiakotron@MartyFiakOTron-dellg155510  ~/TP-LINUX-B2/tp5  vagrant up

Bringing machine 'web1.tp5.b2' up with 'virtualbox' provider...
Bringing machine 'rp1.tp5.b2' up with 'virtualbox' provider...
Bringing machine 'db1.tp5.b2' up with 'virtualbox' provider...
==> web1.tp5.b2: Checking if box 'generic/rocky9' version '4.3.12' is up to date...
==> web1.tp5.b2: Clearing any previously set network interfaces...
==> web1.tp5.b2: Preparing network interfaces based on configuration...
    web1.tp5.b2: Adapter 1: nat
    web1.tp5.b2: Adapter 2: hostonly
==> web1.tp5.b2: Forwarding ports...
    web1.tp5.b2: 22 (guest) => 2222 (host) (adapter 1)
==> web1.tp5.b2: Running 'pre-boot' VM customizations...
==> web1.tp5.b2: Booting VM...
==> web1.tp5.b2: Waiting for machine to boot. This may take a few minutes...
    web1.tp5.b2: SSH address: 127.0.0.1:2222
    web1.tp5.b2: SSH username: vagrant
    web1.tp5.b2: SSH auth method: private key
    web1.tp5.b2: Warning: Connection reset. Retrying...
    web1.tp5.b2: 
    web1.tp5.b2: Vagrant insecure key detected. Vagrant will automatically replace
    web1.tp5.b2: this with a newly generated keypair for better security.
    web1.tp5.b2: 
    web1.tp5.b2: Inserting generated public key within guest...
    web1.tp5.b2: Removing insecure key from the guest if it's present...
    web1.tp5.b2: Key inserted! Disconnecting and reconnecting using new SSH key...
==> web1.tp5.b2: Machine booted and ready!
==> web1.tp5.b2: Checking for guest additions in VM...
    web1.tp5.b2: The guest additions on this VM do not match the installed version of
    web1.tp5.b2: VirtualBox! In most cases this is fine, but in rare cases it can
    web1.tp5.b2: prevent things such as shared folders from working properly. If you see
    web1.tp5.b2: shared folder errors, please make sure the guest additions within the
    web1.tp5.b2: virtual machine match the version of VirtualBox you have installed on
    web1.tp5.b2: your host and reload your VM.
    web1.tp5.b2: 
    web1.tp5.b2: Guest Additions Version: 6.1.48
    web1.tp5.b2: VirtualBox Version: 7.0
==> web1.tp5.b2: Setting hostname...
==> web1.tp5.b2: Configuring and enabling network interfaces...
==> rp1.tp5.b2: Importing base box 'generic/rocky9'...
==> rp1.tp5.b2: Matching MAC address for NAT networking...
==> rp1.tp5.b2: Checking if box 'generic/rocky9' version '4.3.12' is up to date...
==> rp1.tp5.b2: Setting the name of the VM: rp1.tp5.b2
==> rp1.tp5.b2: Fixed port collision for 22 => 2222. Now on port 2200.
==> rp1.tp5.b2: Clearing any previously set network interfaces...
==> rp1.tp5.b2: Preparing network interfaces based on configuration...
    rp1.tp5.b2: Adapter 1: nat
    rp1.tp5.b2: Adapter 2: hostonly
==> rp1.tp5.b2: Forwarding ports...
    rp1.tp5.b2: 22 (guest) => 2200 (host) (adapter 1)
==> rp1.tp5.b2: Running 'pre-boot' VM customizations...
==> rp1.tp5.b2: Booting VM...
==> rp1.tp5.b2: Waiting for machine to boot. This may take a few minutes...
    rp1.tp5.b2: SSH address: 127.0.0.1:2200
    rp1.tp5.b2: SSH username: vagrant
    rp1.tp5.b2: SSH auth method: private key
    rp1.tp5.b2: Warning: Connection reset. Retrying...
    rp1.tp5.b2: 
    rp1.tp5.b2: Vagrant insecure key detected. Vagrant will automatically replace
    rp1.tp5.b2: this with a newly generated keypair for better security.
    rp1.tp5.b2: 
    rp1.tp5.b2: Inserting generated public key within guest...
    rp1.tp5.b2: Removing insecure key from the guest if it's present...
    rp1.tp5.b2: Key inserted! Disconnecting and reconnecting using new SSH key...
==> rp1.tp5.b2: Machine booted and ready!
==> rp1.tp5.b2: Checking for guest additions in VM...
    rp1.tp5.b2: The guest additions on this VM do not match the installed version of
    rp1.tp5.b2: VirtualBox! In most cases this is fine, but in rare cases it can
    rp1.tp5.b2: prevent things such as shared folders from working properly. If you see
    rp1.tp5.b2: shared folder errors, please make sure the guest additions within the
    rp1.tp5.b2: virtual machine match the version of VirtualBox you have installed on
    rp1.tp5.b2: your host and reload your VM.
    rp1.tp5.b2: 
    rp1.tp5.b2: Guest Additions Version: 6.1.48
    rp1.tp5.b2: VirtualBox Version: 7.0
==> rp1.tp5.b2: Setting hostname...
==> rp1.tp5.b2: Configuring and enabling network interfaces...
==> db1.tp5.b2: Importing base box 'generic/rocky9'...
==> db1.tp5.b2: Matching MAC address for NAT networking...
==> db1.tp5.b2: Checking if box 'generic/rocky9' version '4.3.12' is up to date...
==> db1.tp5.b2: Setting the name of the VM: db1.tp5.b2
==> db1.tp5.b2: Fixed port collision for 22 => 2222. Now on port 2201.
==> db1.tp5.b2: Clearing any previously set network interfaces...
==> db1.tp5.b2: Preparing network interfaces based on configuration...
    db1.tp5.b2: Adapter 1: nat
    db1.tp5.b2: Adapter 2: hostonly
==> db1.tp5.b2: Forwarding ports...
    db1.tp5.b2: 22 (guest) => 2201 (host) (adapter 1)
==> db1.tp5.b2: Running 'pre-boot' VM customizations...
==> db1.tp5.b2: Booting VM...
==> db1.tp5.b2: Waiting for machine to boot. This may take a few minutes...
    db1.tp5.b2: SSH address: 127.0.0.1:2201
    db1.tp5.b2: SSH username: vagrant
    db1.tp5.b2: SSH auth method: private key
    db1.tp5.b2: 
    db1.tp5.b2: Vagrant insecure key detected. Vagrant will automatically replace
    db1.tp5.b2: this with a newly generated keypair for better security.
    db1.tp5.b2: 
    db1.tp5.b2: Inserting generated public key within guest...
    db1.tp5.b2: Removing insecure key from the guest if it's present...
    db1.tp5.b2: Key inserted! Disconnecting and reconnecting using new SSH key...
==> db1.tp5.b2: Machine booted and ready!
==> db1.tp5.b2: Checking for guest additions in VM...
    db1.tp5.b2: The guest additions on this VM do not match the installed version of
    db1.tp5.b2: VirtualBox! In most cases this is fine, but in rare cases it can
    db1.tp5.b2: prevent things such as shared folders from working properly. If you see
    db1.tp5.b2: shared folder errors, please make sure the guest additions within the
    db1.tp5.b2: virtual machine match the version of VirtualBox you have installed on
    db1.tp5.b2: your host and reload your VM.
    db1.tp5.b2: 
    db1.tp5.b2: Guest Additions Version: 6.1.48
    db1.tp5.b2: VirtualBox Version: 7.0
==> db1.tp5.b2: Setting hostname...
==> db1.tp5.b2: Configuring and enabling network interfaces...
```

```
 martyfiakotron@MartyFiakOTron-dellg155510  ~/TP-LINUX-B2/tp5  vagrant ssh web1.tp5.b2
```

```
[vagrant@web1 ~]$ vim config_web.sh
```

```
[vagrant@web1 ~]$ cat config_web.sh 
#!/bin/bash

sudo dnf update
sudo dnf install apache2 php libapache2-mod-php -y

# Configurer le fichier de configuration d'Apache
sudo cat > /etc/apache2/sites-available/app_nulle.conf << EOF
<VirtualHost *:80>
    ServerName app_nulle.tp5.b2
    DocumentRoot /var/www/app_nulle

    <Directory /var/www/app_nulle>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF

sudo a2dissite 000-default.conf
sudo a2ensite app_nulle.conf

sudo systemctl restart apache2

sudo docker run -d -p 80:80 --name app_nulle_container your_docker_image

echo "Configuration du serveur web terminée."
```

```
[vagrant@web1 ~]$ sudo sh config_web.sh 
```

```
[vagrant@rp1 ~]$ vim config_rp.sh
```

```
[vagrant@rp1 ~]$ cat config_rp.sh 
#!/bin/bash

sudo dnf update
sudo dnf install nginx -y

sudo cat > /etc/nginx/sites-available/app_nulle.conf << EOF
server {
    listen 80;
    server_name app_nulle.tp5.b2;

    location / {
        proxy_pass http://10.5.1.11;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
    }
}
EOF

sudo unlink /etc/nginx/sites-enabled/default
sudo ln -s /etc/nginx/sites-available/app_nulle.conf /etc/nginx/sites-enabled/
sudo systemctl restart nginx

echo "Configuration du reverse proxy terminée."
```

```
[vagrant@rp1 ~]$ sudo sh config_rp.sh 
```

```
[vagrant@db1 ~]$ vim config_db.sh
```
```
[vagrant@db1 ~]$ cat config_db.sh 
#!/bin/bash

sudo dnf update
sudo dnf install mariadb-server -y

sudo sed -i 's/bind-address.*/bind-address = 127.0.0.1/' /etc/mysql/mariadb.conf.d/50-server.cnf

sudo systemctl restart mariadb

mysql -u root -p <<MYSQL_SCRIPT
CREATE DATABASE app_nulle;
CREATE USER 'utilisateur'@'10.5.1.11' IDENTIFIED BY 'mot_de_passe';
GRANT ALL PRIVILEGES ON app_nulle.* TO 'utilisateur'@'10.5.1.11';
FLUSH PRIVILEGES;
MYSQL_SCRIPT

echo "Configuration de la base de données terminée."

```

```
[vagrant@db1 ~]$ sudo sh config_db.sh 
```